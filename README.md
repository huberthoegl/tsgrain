Tsgrain
=======

Hubert Högl, March 2024, <Hubert.Hoegl@hs-augsburg.de>


This is a tennis court irrigation controller for TSG Stadtbergen (Germany). The
current hardware setup allows to manually control irrigition of up to seven
tennis courts. It can easily be expanded by cheap hardware to more than seven
courts.  The Tsgrain project consists of two parts, a hardware part which 
interfaces the Raspberry Pi to external hardware, and a software part. 

The repository contains Python 3 software which runs on a *Raspberry Pi 3B
Plus* under Debian Linux or on a normal *Linux PC* with an arbitrary Linux
Distribution. 

This work is based on the bachelor thesis of Christian Graber, titled
"Entwicklung einer Zeitsteuerung zur Bewässerung von Tennisanlagen mittels
lokaler Steuerung und Web-Oberfläche", Technische Hochschule Augsburg, 2020
(supervised by me). In 2022 the software was completely rewritten by Thomas
Hampp as part of his bachelor thesis.  This code is available at 
[code.thetadev.de](https://code.thetadev.de/TSGRain) in the repos "Controller",
"WebUI" (based on Go in the backend and React in Frontend), "yocto_os" and
"SEBRAUC". 

Finally from January to March 2024 I again rewrote parts of the software and 
added a few new features. 

The main software parts are all written in Python:

- Controller process 
- Web-UI process (Flask, Bootstrap, XHR)
- CLI client 

Most of the very sound architectural decision of Thomas Hampp I adopted, e.g.
the Controller code and gRPC communication. The Web-UI backend I rewrote in
Python (tradition HTTP requests with Flask), in the frontend I use
Bootstrap, Wtforms, Jinja2 templates  and some simple JavaScript for XHR
requests to asynchronously update GUI elements and minor other things. The
React Web-UI by Thomas Hampp was actually very impressive, but as a React newby
I chose a traditional technology I am more familiar with.  



Hardware 
--------

See the [schematics](doc/schaltplan.pdf).

The RPi3 must be connected to three external hardware components. All of them
are driven by the same I2C bus, pins 3 (SDA) and 5 (SCL) of the RPi:

1. MCP23017 I/O Expander Nr. 1 (I2C address 0x27)

   This expander drives up to eight relais with opto-couplers on PORTB. In our
   case the relais drive 24V AC magnetic valves which open the water inflow to
   irrigate the courts.  Between the PORTB signals and the relais inputs you
   can optionally add up to eight switches each with three ways: constantly
   off, constantly on, and control via RPi.

   PORTA is used to drive LEDs which reflect the status of the relais outputs
   (common cathode).

   No interrupt lines are used.


2. MCP23017 I/O Expander Nr. 2 (I2C address 0x23)

   This expander reads up to eight push buttons on PORTA.  The buttons are 
   active low when pushed. The interrupt line of the MCP is connected to RPi 
   GPIO17.

   PORTB is used to drive a RGB-LED to display the status of the controller 
   (common cathode).


3. DS3231 Real-Time Clock  (I2C address 0x68)

   The battery-backed RTC clock is used to set the system time after rebooting
   the RPi. No interrupt line is used.
   
Finally a single push button is connected (active low) to GPIO22. This button
is used as a reboot/shutdown button.

In summary, you need only one I2C bus (SCL, SDA) and two GPIO pins to interface
all the needed external hardware. In principle it would be no problem to move
from the RPi to a similar Embedded Linux hardware, e.g. the *Beaglebone Black*.

A part list is [here](doc/Teileliste.md).


Installation 
------------

**Virtual Environment**

Create a virtual environment for Python on PC or RPi:

```
# create env
python -m venv python_venv
# activate env
. python_venv/bin/activate
# upgrade pip
pip install -U pip
# install requirements
pip install -r requirements.txt 
```

When running on RPi choose `requirements_rpi.txt`.


Set PYTHONPATH environment variable to the directory where this `README.md` is
in.  



**Controller**

When running on PC

```
# print help
python -m tsgrain_controller --help
```

Start controller either with `--io NONE` or with `--io MOCKUI`

With MOCKUI your need to start the Python program `tsgrain_mockui.py` (tkinter):

```
python -m tsgrain_controller --config tsgrain.toml --io MOCKUI

python tsgrain_mockui.py
```

See the [screenshot](doc/tsgrain_mockui.png) of the small 
Python/Tkinter program `tsgrain_mockui.py`. 


When running on RPi with peripheral hardware choose

```
python -m tsgrain_controller --config tsgrain.toml --io MCP239017
```

Configuration parameters are contained in file `tsgrain.toml`. You may 
change these values and store it under another filename, e.g. see 
`tsgrain_v2.toml`.


**CLI Client**


```
cd cli_client
python tsgrain_cli.py --help
```


**Web Application**


```
$ cd web/
$ python -m flask run --host=0.0.0.0
```


Documentation
-------------

A nice documentation is still missing (but I am working on it). There is a [small
article](https://www.tha.de/Informatik/Moderne-Beregnungssteuerung-fuer-Tennisanlagen.html)
from 2020 (the Web-UI screenshots are outdated). New Web-UI and other images are 
[here](https://hhoegl.de/tsgrain/sigal/_build/index.html).

See the [Tsgrain homepage](https://hhoegl.de/tsgrain).


