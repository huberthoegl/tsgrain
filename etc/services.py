#! /usr/bin/python3

import os, sys

SERVICES = ("tsgrain_controller.service", "tsgrain_website.service")

def usage():
    print('''usage: services.py status|stop|start|enable|disable
''')
    os._exit(0)


def main():
    if len(sys.argv) != 2:
        usage()
    else:
        op = sys.argv[1]

    for svc in SERVICES:  
        cmd = "sudo systemctl --no-pager {} {}".format(op, svc)
        os.system(cmd)
    

if __name__ == "__main__":
    main()

