
'''Testprogramm fuer die Beregnungs-Tasten und die LEDs (ehem. kbdtest.py)

Die Konfiguration der beiden MCP Bausteine ist noch fuer die erste Prototyp Box
vom Juli 2021.  Jedoch liegen auch beim neueren Testsystem die Zonentasten an
MCP 0x23, Port A. Deshalb kann man das Programm zum Test der Eingabetasten bei
beiden Varianten verwenden.

H. Hoegl, Maerz 2024

'''


'''
Register Dump, siehe dump_reg(...)

0x23 IODIRA   (0x00) 0b11111111
0x23 IODIRB   (0x01) 0b00000000
0x23 IPOLA    (0x02) 0b00000000
0x23 IPOLB    (0x03) 0b00000000
0x23 GPINTENA (0x04) 0b11111111
0x23 GPINTENB (0x05) 0b00000000
0x23 DEFVALA  (0x06) 0b11111111
0x23 DEFVALB  (0x07) 0b00000000
0x23 INTCONA  (0x08) 0b11111111
0x23 INTCONB  (0x09) 0b00000000
0x23 IOCON    (0x0a) 0b01111010
0x23 GPPUA    (0x0c) 0b11111111
0x23 GPPUB    (0x0d) 0b00000000
0x23 INTFA    (0x0e) 0b00000000
0x23 INTFB    (0x0f) 0b00000000
0x23 INTCAPA  (0x10) 0b11011111
0x23 INTCAPB  (0x11) 0b00000000
0x23 GPIOA    (0x12) 0b11111111
0x23 GPIOB    (0x13) 0b00000000
0x23 OLATA    (0x14) 0b00000000
0x23 OLATB    (0x15) 0b00000000
'''

import RPi.GPIO as GPIO
import time
import smbus
import sys

presscount = 0
counter = 0

PB1 = 'PB1'
PB2 = 'PB2'
PB3 = 'PB3'
PB4 = 'PB4'
PB5 = 'PB5'
PB6 = 'PB6'
PB7 = 'PB7'
PBAutoOff = 'PBAutoOff'


def press_handler(intnr, keynr):
    global presscount
    presscount += 1
    print("press_handler", keynr)


def release_handler(intnr, keynr):
    global presscount
    presscount -= 1
    print("release_handler", keynr)



def mcp_handler2(intr_nr):
    '''Interrupt Service Routine fuer I2C Expander 2, PORTA

    Diese Variante erzeugt nur einen Interrupt beim Druecken der Taste. Das Loslassen wird
    durch Polling festgestellt.

    GPIO.add_event_detect(17,
                          GPIO.RISING,
                          callback=mcp_handler2)   # omitted bouncetime!

    Initialisierung des MCP:
    bus.write_byte_data(expand_2, INTCONA_2, 0xFF) # all pins compared against defval
    '''
    global counter
    intfa = bus.read_byte_data(expand_2, INTFA_2)    # first read INTFA, then ...
    bus.write_byte_data(expand_2, GPINTENA_2, 0x00)  # ... disable MCP INTRs (clears INTFA!)

    time.sleep(0.1)

    print("{} {} intfa={:x}".format(counter, presscount, intfa))

    capa = bus.read_byte_data(expand_2, INTCAPA_2)  # clears intr flag
    # print("capa={:x}".format(capa))
    if capa == 0xfe:
        bt = PB1
        press_handler(intr_nr, bt)
    elif capa == 0xfd:
        bt = PB2
        press_handler(intr_nr, bt)
    elif capa == 0xfb:
        bt = PB3
        press_handler(intr_nr, bt)
    elif capa == 0xf7:
        bt = PB4
        press_handler(intr_nr, bt)
    elif capa == 0xef:
        bt = PB5
        press_handler(intr_nr, bt)
    elif capa == 0xdf:
        bt = PB6
        press_handler(intr_nr, bt)
    elif capa == 0xbf:
        bt = PB7
        press_handler(intr_nr, bt)
    elif capa == 0x7f:
        bt = PBAutoOff
        press_handler(intr_nr, bt)

    while True:
        # wait for button release
        gpa = bus.read_byte_data(expand_2, GPIOA_2)   # clears intr flag
        # print("gpa={:x}".format(gpa))
        if gpa == 0xff:
            release_handler(intr_nr, bt)
            break
        time.sleep(0.1)

    counter += 1
    bus.write_byte_data(expand_2, GPINTENA_2, 0xff)  # enable interrupts



def mcp_handler(intr_nr):
    '''Interrupt Service Routine fuer I2C Expander 2, PORTA

    Haengt gelegentlich nach der folgenden Logzeile:
    2020-06-02 15:48:27,934 - kbd-logger - INFO - mcp_handler: read_interrupt NONE KEY
    Danach erzeugt der MCP keinen Interrupt mehr.

    Initialisierung des MCP:
    bus.write_byte_data(expand_2, INTCONA_2, 0x00) #1: Compared agains previous pin value

    Initialisierung des RPi Interrupt-Eingang:
    GPIO.add_event_detect(17,
                          GPIO.RISING,
                          callback=mcp_handler, bouncetime=100)

    '''
    key_nr = read_interrupt()
    if key_nr == None:
        gpa = read_tasten()  # dummy read to clear interrupt condition (1.6.20)
        bus.write_byte_data(expand_2, GPINTENA_2, 0xff)  # enable interrupts
        return  # key could not be identified
    time.sleep(0.1)
    capa = read_intcapa()
    if capa == 255:
        release_handler(intr_nr, key_nr)
        gpa = read_tasten()  # dummy read to clear interrupt condition (1.6.20)
    else:
        if capa == 0xfe: # P1
            press_handler(intr_nr, key_nr)
        elif capa == 0xfd: # P2
            press_handler(intr_nr, key_nr)
        elif capa == 0xfb: # P3
            press_handler(intr_nr, key_nr)
        elif capa == 0xf7: # P4
            press_handler(intr_nr, key_nr)
        elif capa == 0xef: # P5
            press_handler(intr_nr, key_nr)
        elif capa == 0xdf: # P6
            press_handler(intr_nr, key_nr)
        elif capa == 0xbf: # P7
            press_handler(intr_nr, key_nr)
        elif capa == 0x7f: # PBAutoOff
            press_handler(intr_nr, key_nr)
        gpa = read_tasten()  # dummy read to clear interrupt condition (1.6.20)



def init():
    global expand_1, expand_2, OLATA_1, OLATB_1, OLATA_2, OLATB_2, INTFA_2, \
           GPIOA_2, GPINTENA_2, INTCAPA_2

    #Konfiguration I2C
    ##### Erweiterungsboard 1 -- Output-Platine ###############################

    expand_1 = 0x27 # i2c Adresse
    IODIRA_1 = 0x00 # Pin Register
    IODIRB_1 = 0x01 # Pin Register
    OLATA_1 = 0x14 # Register fuer GPA Ausgabe
    OLATB_1 = 0x15 # Register fuer GPB Ausgabe

    # Definiere GPA/OLATA und GPB/OLATB als Output // 1 für Ausgabe,
    # 0 für Eingabe
    bus.write_byte_data(expand_1, IODIRA_1, 0x00)
    bus.write_byte_data(expand_1, IODIRB_1, 0x00)

    # GPA/OLATA und GPB/OLATB default Werte setzen
    bus.write_byte_data(expand_1, OLATA_1, 0x00)
    bus.write_byte_data(expand_1, OLATB_1, 0xFF)

    ##### Erweiterungsboard 2 -- Input-Platine #################################
    expand_2 = 0x23 # i2c Adresse
    IODIRA_2 = 0x00 # Pin Register
    IODIRB_2 = 0x01 # Pin Register
    GPPUA_2 = 0x0C # Register für Pull-Up Widerstände
    GPIOA_2 = 0x12 # Register fuer GPIOA Eingabe
    OLATB_2 = 0x15 # Register fuer GPIOB Ausgabe

    # Definiere GPA als Input und GPB als Output
    # Alle 8 GPA sind als Input und Interrupt eingestellt
    bus.write_byte_data(expand_2, IODIRA_2, 0xFF)
    bus.write_byte_data(expand_2, IODIRB_2, 0x00)

    # GPB als Output deklariert für Status LED
    bus.write_byte_data(expand_2, OLATB_2, 0x00)

    bus.write_byte_data(expand_2, GPPUA_2, 0xFF)

    # Konfiguration für Interrupt Board2-GPB
    IOCONA_2 = 0x0A # Konfigurationsregister für INTPOL
    INTCONA_2 = 0x08 # Interrupt auf Abweichung vom Standartwert festlegen
    DEFVALA_2 = 0x06 # Standartwert des Pins
    GPINTENA_2 = 0x04 # Interrupt für welchen Pin festlegen
    INTCAPA_2 = 0x10 # Interrupt Captured Register
    INTFA_2 = 0x0E # Interrupt Flag register

    bus.write_byte_data(expand_2, GPINTENA_2, 0xFF) #pins on interrupt
    bus.write_byte_data(expand_2, DEFVALA_2, 0xFF) #standartwert 1
    bus.write_byte_data(expand_2, INTCONA_2, 0xFF) #1: Compared against defval
    # bus.write_byte_data(expand_2, INTCONA_2, 0x00) #1: Compared agains previous pin value

    iocon = bus.read_byte_data(expand_2, IOCONA_2) #IOCON Register bearbeiten
    iocon = iocon | 0b01111010
    # Die Steuerung am Tennisheim benoetigt unbedingt, dass beide Interrupts
    # INTA und INTB gespiegelt werden (iocon = 0x7a; mit iocon = 0x3a
    # funktioniert es nicht!
    bus.write_byte_data(expand_2, IOCONA_2, iocon)

    # Starten des Interrupts entweder durch Auslesen GPIO oder durch Auslesen
    # INTCAP
    Gpio_wert = bus.read_byte_data(expand_2, GPIOA_2)

    # IPOLA to 0x00
    bus.write_byte_data(expand_2, 0x02, 0x00)


def read_interrupt():
    '''Gibt zurueck, welche Taste gedrueckt wurde (0...7).
    '''
    # The INTF register reflects the interrupt condition on the
    # port pins of any pin that is enabled for interrupts via the
    # GPINTEN register. A set bit indicates that the
    # associated pin caused the interrupt.
    irwert = bus.read_byte_data(expand_2, INTFA_2)
    mask = 0x01
    i = 0
    while i < 8:
        if mask & irwert:
            break
        else:
            mask <<= 1
            i += 1
    if 0 <= i <= 7:
        return i
    else:
        return None


def read_tasten():
    '''Read the value on the port GPIOA.

    Manual: "The interrupt condition is cleared after the LSB of the data
    is clocked out during a read command of GPIO or INTCAP."
    '''
    return bus.read_byte_data(expand_2, GPIOA_2)



def read_intcapa():
    '''INTCAP captures the GPIO port value at the time the interrupt
    occured.

    Manual: "The interrupt condition is cleared after the LSB of the
    data is clocked out during a read command of GPIO or INTCAP."
    '''
    return bus.read_byte_data(expand_2, INTCAPA_2)


def dump_reg(name, i2c_addr, reg_addr):
    data = bus.read_byte_data(i2c_addr, reg_addr)
    print("0x{:x} {:8s} (0x{:02x}) 0b{:08b}".format(i2c_addr, name, reg_addr, data))


def main1():

    for i in range(5):
        wert = 0xff
        bus.write_byte_data(expand_2, OLATB_2, wert)
        bus.write_byte_data(expand_1, OLATB_1, 0x00)
        time.sleep(0.2)
        wert = 0x00
        bus.write_byte_data(expand_2, OLATB_2, wert)
        bus.write_byte_data(expand_1, OLATB_1, 0xff)
        time.sleep(0.2)

    print("output loop done")

    # GPIO17 to interrupt output of MCP23017
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    GPIO.add_event_detect(17,
                          GPIO.RISING,
                          callback=mcp_handler2)

    dump_reg('IODIRA', 0x23, 0x00)
    dump_reg('IODIRB', 0x23, 0x01)
    dump_reg('IPOLA', 0x23, 0x02)
    dump_reg('IPOLB', 0x23, 0x03)
    dump_reg('GPINTENA', 0x23, 0x04)
    dump_reg('GPINTENB', 0x23, 0x05)
    dump_reg('DEFVALA', 0x23, 0x06)
    dump_reg('DEFVALB', 0x23, 0x07)
    dump_reg('INTCONA', 0x23, 0x08)
    dump_reg('INTCONB', 0x23, 0x09)
    dump_reg('IOCON', 0x23, 0x0A)  # also on addr 0x0B!
    dump_reg('GPPUA', 0x23, 0x0C)
    dump_reg('GPPUB', 0x23, 0x0D)
    dump_reg('INTFA', 0x23, 0x0E)
    dump_reg('INTFB', 0x23, 0x0F)
    dump_reg('INTCAPA', 0x23, 0x10)
    dump_reg('INTCAPB', 0x23, 0x11)
    dump_reg('GPIOA', 0x23, 0x12)
    dump_reg('GPIOB', 0x23, 0x13)
    dump_reg('OLATA', 0x23, 0x14)
    dump_reg('OLATB', 0x23, 0x15)
    print()
    print("Nun Zonentasten druecken ...")
    while True:
        time.sleep(1)

# --colorled-new
def main2():
    gpioa = 0x12
    while True:
        print("aus")
        bus.write_byte_data(0x27, gpioa, 0x00)
        time.sleep(2)

        print("bit 5 an - rot?")
        bus.write_byte_data(0x27, gpioa, 0x20)
        time.sleep(2)

        print("bit 6 an - gruen?")
        bus.write_byte_data(0x27, gpioa, 0x40)
        time.sleep(2)

        print("bit 7 an - blau?")
        bus.write_byte_data(0x27, gpioa, 0x80)
        time.sleep(2)




# --colorled-old
def main3():
    gpiob = 0x13
    while True:
        print("aus")
        bus.write_byte_data(0x23, gpiob, 0x00)
        time.sleep(2)

        print("bit 0 an - rot")
        bus.write_byte_data(0x23, gpiob, 0x01)
        time.sleep(2)

        print("bit 1 an - blau")
        bus.write_byte_data(0x23, gpiob, 0x02)
        time.sleep(2)

        print("bit 2 an - gruen")
        bus.write_byte_data(0x23, gpiob, 0x04)
        time.sleep(2)


def usage():
    print('''usage
   -h               Help
   --zones          Blink zone and output LEDs and enter loop to display zone
                    interrupts (old & new schema)
   --colorled-new   Test Color LED (new schema)
   --colorled-old   Test Color LED (old schema)
''')


if __name__ == "__main__":
    bus = smbus.SMBus(1)
    init()
    if len(sys.argv) == 1 or sys.argv[1] == '-h':
        usage()
    elif sys.argv[1] == "--zones":
        main1()
    elif sys.argv[1] == "--colorled-new":
        main2()
    elif sys.argv[1] == "--colorled-old":
        main3()
    else:
        print("unbekanntes Kommando")
