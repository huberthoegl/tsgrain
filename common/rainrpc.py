from enum import Enum
from typing import List

import grpc
from grpc._channel import _InactiveRpcError
from google.protobuf import empty_pb2
from google.protobuf.wrappers_pb2 import Int32Value
from google.protobuf.wrappers_pb2 import BoolValue

from common.grpc_generated import tsgrain_pb2
from common.grpc_generated import tsgrain_pb2_grpc
from common.grpc_generated.tsgrain_pb2 import (
    TaskRequest,
    TaskSource,
    TaskStart,
    TaskStop,
    Job,
    Timestamp,
    WeekDay,
)


CHANNEL = None
SRV = "localhost:50051"
DFLT_TIME = 300
NZONES = 7


class Source(Enum):
    manual = "manual"
    schedule = "schedule"

    def __str__(self):
        return self.value


class Weekdays(Enum):
    Mon = "Mon"
    Tue = "Tue"
    Wed = "Wed"
    Thu = "Thu"
    Fri = "Fri"
    Sat = "Sat"
    Sun = "Sun"
    All = "All"  # every day Mon to Sun

    def __str__(self):
        return self.value


def _weekday_convert(L):
    R = []
    for day in L:
        if day == Weekdays.Mon:
            R.append(tsgrain_pb2.WeekDay.MON)
        elif day == Weekdays.Tue:
            R.append(tsgrain_pb2.WeekDay.TUE)
        elif day == Weekdays.Wed:
            R.append(tsgrain_pb2.WeekDay.WED)
        elif day == Weekdays.Thu:
            R.append(tsgrain_pb2.WeekDay.THU)
        elif day == Weekdays.Fri:
            R.append(tsgrain_pb2.WeekDay.FRI)
        elif day == Weekdays.Sat:
            R.append(tsgrain_pb2.WeekDay.SAT)
        elif day == Weekdays.Sun:
            R.append(tsgrain_pb2.WeekDay.SUN)
        elif day == Weekdays.All:
            R.append(tsgrain_pb2.WeekDay.ALL)
    return R


def _zones_convert(zones: List[str]):
    """
    Convert a list of strings to a list of zone numbers

    :param zones: some examples are  ["all"], ["1-7"], ["1", "2", "3"], ["1-3,6"]
    ["1-3,5-7"].  You can also write "all", "All" and "ALL".
    """
    import re

    rex = "(\d+)-(\d+)"
    L = []
    for e in zones:
        if e in ("all", "All", "ALL"):
            L.extend(map(str, range(1, NZONES + 1)))
            continue
        if "," in e:
            c = e.split(",")
            L.extend(c)
        else:
            L.append(e)
    R = []
    print("L:", L)
    for x in L:
        mo = re.match(rex, x)
        if mo:
            R.extend(map(str, range(int(mo.group(1)), int(mo.group(2)) + 1)))
        else:
            R.append(x)
    s = set(R)
    z = list(map(int, sorted(list(set(R)))))
    print("_zones_convert:", z)
    return z


def open():
    global CHANNEL
    CHANNEL = grpc.insecure_channel(SRV)
    stub = tsgrain_pb2_grpc.TSGRainStub(CHANNEL)
    return stub


def close():
    CHANNEL.close()


def set_auto_mode(stub, b):
    r = stub.SetAutoMode(BoolValue(value=b))
    print(r)


def get_mode(stub):
    r = stub.GetAutoMode(empty_pb2.Empty())
    print(r.value)


def get_all_tasks(stub):
    TaskList = stub.GetTasks(empty_pb2.Empty())
    return TaskList


def get_all_jobs(stub):
    jobs = stub.GetJobs(empty_pb2.Empty())
    return jobs


def get_job(stub, jobid: int):
    print("get_job:")
    job = stub.GetJob(Int32Value(value=jobid))
    return job


def delete_job(stub, jobid: int):
    """Delete job with jobid. The job is also deleted from the JSON db."""
    empty = stub.DeleteJob(Int32Value(value=jobid))


def enable_job(stub, jobid: int):
    empty = stub.EnableJob(Int32Value(value=jobid))


def disable_job(stub, jobid: int):
    empty = stub.DisableJob(Int32Value(value=jobid))


def start_task(stub, src="manual", zone=1, duration=DFLT_TIME, queuing=True):
    """
    Beim Starten von mehreren Tasks mit der gleichen Zone und queuing True werden
    diese nicht nacheinander ausgefuehrt, sondern nur die erste Task, dann ist
    Schluss.

    Bei unterschiedlichen Zonen werden aufeinanderfolgende Tasks nur dann
    nacheinander ausgefuehrt, wenn queuing True war.  Mit queuing False wird
    nur die erste Task ausgefuehrt.

    Das Gesagte gilt, wenn man weitere Tasks abschickt, waehrend die erste Task
    immer noch laeuft.
    """
    s = TaskSource.MANUAL if src == "manual" else TaskSource.SCHEDULE
    t = TaskStart(source=s, zone_id=zone, duration=duration, queuing=queuing)
    r = stub.StartTask(t)
    return r


def stop_task(stub, src="manual", zone=1):
    s = TaskSource.MANUAL if src == "manual" else TaskSource.SCHEDULE
    t = TaskStop(source=s, zone_id=zone)
    r = stub.StopTask(t)
    return r


def request_task(
    stub, src="manual", zone=1, duration=DFLT_TIME, queuing=False, cancelling=False
):
    """
    Falls man zwei Aufrufe nacheinander absetzt mit cancelling
    True, dann wird beim zweiten Aufruf die laufende Task
    wieder geloescht. So ist das Verhalten auch jetzt bei der
    alten Steuerung.
    """
    s = TaskSource.MANUAL if src == "manual" else TaskSource.SCHEDULE
    req = TaskRequest(
        source=s,
        zone_id=zone,
        duration=duration,
        queuing=queuing,
        cancelling=cancelling,
    )
    r = stub.RequestTask(req)
    return r


def create_job(
    stub,
    id: int,
    starttime: int,  # seconds
    duration: int,
    zones: list[int],
    enable: bool,
    repeat: bool,
    weekdays: list[Weekdays],
    except_days: list[str],
):
    """
    zones -- List of int 1-7

    """
    job = Job(
        id=id,
        date=Timestamp(seconds=starttime),
        duration=duration,
        zones=zones,
        enable=enable,
        repeat=repeat,
        weekdays=_weekday_convert(weekdays),
        except_days=except_days,
    )
    r = stub.CreateJob(job)
    print("CreateJob returned", r)


def update_job(
    stub,
    id: int,
    starttime: int,  # seconds
    duration: int,
    zones: list[int],
    enable: bool,
    repeat: bool,
    weekdays: list[Weekdays],
    except_days: list[str],
):
    job = Job(
        id=id,
        date=Timestamp(seconds=starttime),
        duration=duration,
        zones=zones,
        enable=enable,
        repeat=repeat,
        weekdays=_weekday_convert(weekdays),
        except_days=except_days,
    )
    empty = stub.UpdateJob(job)


def get_def_irr_time(stub):
    response = stub.GetDefaultIrrigationTime(empty_pb2.Empty())
    return response
