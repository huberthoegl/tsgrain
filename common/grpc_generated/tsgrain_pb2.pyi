from google.protobuf import empty_pb2 as _empty_pb2
from google.protobuf import wrappers_pb2 as _wrappers_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class TaskSource(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = ()
    MANUAL: _ClassVar[TaskSource]
    SCHEDULE: _ClassVar[TaskSource]

class WeekDay(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = ()
    MON: _ClassVar[WeekDay]
    TUE: _ClassVar[WeekDay]
    WED: _ClassVar[WeekDay]
    THU: _ClassVar[WeekDay]
    FRI: _ClassVar[WeekDay]
    SAT: _ClassVar[WeekDay]
    SUN: _ClassVar[WeekDay]
    ALL: _ClassVar[WeekDay]
MANUAL: TaskSource
SCHEDULE: TaskSource
MON: WeekDay
TUE: WeekDay
WED: WeekDay
THU: WeekDay
FRI: WeekDay
SAT: WeekDay
SUN: WeekDay
ALL: WeekDay

class Timestamp(_message.Message):
    __slots__ = ("seconds",)
    SECONDS_FIELD_NUMBER: _ClassVar[int]
    seconds: int
    def __init__(self, seconds: _Optional[int] = ...) -> None: ...

class TaskRequest(_message.Message):
    __slots__ = ("source", "zone_id", "duration", "queuing", "cancelling")
    SOURCE_FIELD_NUMBER: _ClassVar[int]
    ZONE_ID_FIELD_NUMBER: _ClassVar[int]
    DURATION_FIELD_NUMBER: _ClassVar[int]
    QUEUING_FIELD_NUMBER: _ClassVar[int]
    CANCELLING_FIELD_NUMBER: _ClassVar[int]
    source: TaskSource
    zone_id: int
    duration: int
    queuing: bool
    cancelling: bool
    def __init__(self, source: _Optional[_Union[TaskSource, str]] = ..., zone_id: _Optional[int] = ..., duration: _Optional[int] = ..., queuing: bool = ..., cancelling: bool = ...) -> None: ...

class TaskRequestResult(_message.Message):
    __slots__ = ("started", "stopped")
    STARTED_FIELD_NUMBER: _ClassVar[int]
    STOPPED_FIELD_NUMBER: _ClassVar[int]
    started: bool
    stopped: bool
    def __init__(self, started: bool = ..., stopped: bool = ...) -> None: ...

class TaskStart(_message.Message):
    __slots__ = ("source", "zone_id", "duration", "queuing")
    SOURCE_FIELD_NUMBER: _ClassVar[int]
    ZONE_ID_FIELD_NUMBER: _ClassVar[int]
    DURATION_FIELD_NUMBER: _ClassVar[int]
    QUEUING_FIELD_NUMBER: _ClassVar[int]
    source: TaskSource
    zone_id: int
    duration: int
    queuing: bool
    def __init__(self, source: _Optional[_Union[TaskSource, str]] = ..., zone_id: _Optional[int] = ..., duration: _Optional[int] = ..., queuing: bool = ...) -> None: ...

class TaskStop(_message.Message):
    __slots__ = ("source", "zone_id")
    SOURCE_FIELD_NUMBER: _ClassVar[int]
    ZONE_ID_FIELD_NUMBER: _ClassVar[int]
    source: TaskSource
    zone_id: int
    def __init__(self, source: _Optional[_Union[TaskSource, str]] = ..., zone_id: _Optional[int] = ...) -> None: ...

class Task(_message.Message):
    __slots__ = ("source", "zone_id", "duration", "datetime_started", "datetime_finished")
    SOURCE_FIELD_NUMBER: _ClassVar[int]
    ZONE_ID_FIELD_NUMBER: _ClassVar[int]
    DURATION_FIELD_NUMBER: _ClassVar[int]
    DATETIME_STARTED_FIELD_NUMBER: _ClassVar[int]
    DATETIME_FINISHED_FIELD_NUMBER: _ClassVar[int]
    source: TaskSource
    zone_id: int
    duration: int
    datetime_started: Timestamp
    datetime_finished: Timestamp
    def __init__(self, source: _Optional[_Union[TaskSource, str]] = ..., zone_id: _Optional[int] = ..., duration: _Optional[int] = ..., datetime_started: _Optional[_Union[Timestamp, _Mapping]] = ..., datetime_finished: _Optional[_Union[Timestamp, _Mapping]] = ...) -> None: ...

class TaskList(_message.Message):
    __slots__ = ("tasks", "now", "auto_mode")
    TASKS_FIELD_NUMBER: _ClassVar[int]
    NOW_FIELD_NUMBER: _ClassVar[int]
    AUTO_MODE_FIELD_NUMBER: _ClassVar[int]
    tasks: _containers.RepeatedCompositeFieldContainer[Task]
    now: Timestamp
    auto_mode: bool
    def __init__(self, tasks: _Optional[_Iterable[_Union[Task, _Mapping]]] = ..., now: _Optional[_Union[Timestamp, _Mapping]] = ..., auto_mode: bool = ...) -> None: ...

class Job(_message.Message):
    __slots__ = ("id", "date", "duration", "zones", "enable", "repeat", "weekdays", "except_days")
    ID_FIELD_NUMBER: _ClassVar[int]
    DATE_FIELD_NUMBER: _ClassVar[int]
    DURATION_FIELD_NUMBER: _ClassVar[int]
    ZONES_FIELD_NUMBER: _ClassVar[int]
    ENABLE_FIELD_NUMBER: _ClassVar[int]
    REPEAT_FIELD_NUMBER: _ClassVar[int]
    WEEKDAYS_FIELD_NUMBER: _ClassVar[int]
    EXCEPT_DAYS_FIELD_NUMBER: _ClassVar[int]
    id: int
    date: Timestamp
    duration: int
    zones: _containers.RepeatedScalarFieldContainer[int]
    enable: bool
    repeat: bool
    weekdays: _containers.RepeatedScalarFieldContainer[WeekDay]
    except_days: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, id: _Optional[int] = ..., date: _Optional[_Union[Timestamp, _Mapping]] = ..., duration: _Optional[int] = ..., zones: _Optional[_Iterable[int]] = ..., enable: bool = ..., repeat: bool = ..., weekdays: _Optional[_Iterable[_Union[WeekDay, str]]] = ..., except_days: _Optional[_Iterable[str]] = ...) -> None: ...

class JobID(_message.Message):
    __slots__ = ("id",)
    ID_FIELD_NUMBER: _ClassVar[int]
    id: int
    def __init__(self, id: _Optional[int] = ...) -> None: ...

class JobList(_message.Message):
    __slots__ = ("jobs",)
    JOBS_FIELD_NUMBER: _ClassVar[int]
    jobs: _containers.RepeatedCompositeFieldContainer[Job]
    def __init__(self, jobs: _Optional[_Iterable[_Union[Job, _Mapping]]] = ...) -> None: ...

class SystemTime(_message.Message):
    __slots__ = ("datetime", "timezone")
    DATETIME_FIELD_NUMBER: _ClassVar[int]
    TIMEZONE_FIELD_NUMBER: _ClassVar[int]
    datetime: Timestamp
    timezone: str
    def __init__(self, datetime: _Optional[_Union[Timestamp, _Mapping]] = ..., timezone: _Optional[str] = ...) -> None: ...
