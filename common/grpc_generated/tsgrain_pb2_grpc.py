# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from common.grpc_generated import tsgrain_pb2 as common_dot_grpc__generated_dot_tsgrain__pb2
from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2
from google.protobuf import wrappers_pb2 as google_dot_protobuf_dot_wrappers__pb2


class TSGRainStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.RequestTask = channel.unary_unary(
                '/TSGRain/RequestTask',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskRequest.SerializeToString,
                response_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskRequestResult.FromString,
                )
        self.StartTask = channel.unary_unary(
                '/TSGRain/StartTask',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskStart.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_wrappers__pb2.BoolValue.FromString,
                )
        self.StopTask = channel.unary_unary(
                '/TSGRain/StopTask',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskStop.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_wrappers__pb2.BoolValue.FromString,
                )
        self.GetTasks = channel.unary_unary(
                '/TSGRain/GetTasks',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskList.FromString,
                )
        self.StreamTasks = channel.unary_stream(
                '/TSGRain/StreamTasks',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskList.FromString,
                )
        self.CreateJob = channel.unary_unary(
                '/TSGRain/CreateJob',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.Job.SerializeToString,
                response_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.FromString,
                )
        self.GetJob = channel.unary_unary(
                '/TSGRain/GetJob',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
                response_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.Job.FromString,
                )
        self.GetJobs = channel.unary_unary(
                '/TSGRain/GetJobs',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobList.FromString,
                )
        self.UpdateJob = channel.unary_unary(
                '/TSGRain/UpdateJob',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.Job.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.DeleteJob = channel.unary_unary(
                '/TSGRain/DeleteJob',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.EnableJob = channel.unary_unary(
                '/TSGRain/EnableJob',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.DisableJob = channel.unary_unary(
                '/TSGRain/DisableJob',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.GetAutoMode = channel.unary_unary(
                '/TSGRain/GetAutoMode',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_wrappers__pb2.BoolValue.FromString,
                )
        self.SetAutoMode = channel.unary_unary(
                '/TSGRain/SetAutoMode',
                request_serializer=google_dot_protobuf_dot_wrappers__pb2.BoolValue.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.GetSystemTime = channel.unary_unary(
                '/TSGRain/GetSystemTime',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.SystemTime.FromString,
                )
        self.SetSystemTime = channel.unary_unary(
                '/TSGRain/SetSystemTime',
                request_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.Timestamp.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.SetSystemTimezone = channel.unary_unary(
                '/TSGRain/SetSystemTimezone',
                request_serializer=google_dot_protobuf_dot_wrappers__pb2.StringValue.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.GetDefaultIrrigationTime = channel.unary_unary(
                '/TSGRain/GetDefaultIrrigationTime',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_wrappers__pb2.Int32Value.FromString,
                )
        self.SetDefaultIrrigationTime = channel.unary_unary(
                '/TSGRain/SetDefaultIrrigationTime',
                request_serializer=google_dot_protobuf_dot_wrappers__pb2.Int32Value.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )
        self.GetNZones = channel.unary_unary(
                '/TSGRain/GetNZones',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_wrappers__pb2.Int32Value.FromString,
                )


class TSGRainServicer(object):
    """Missing associated documentation comment in .proto file."""

    def RequestTask(self, request, context):
        """Starte eine neue Bewässerungsaufgabe (oder stoppe eine laufende, wenn
        diese bereits läuft).
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def StartTask(self, request, context):
        """Starte eine Bewässerungsaufgabe
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def StopTask(self, request, context):
        """Stoppe eine Bewässerungsaufgabe
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetTasks(self, request, context):
        """Gibt sämtliche in der Warteschlange befindlichen Bewässerungsaufgaben zurück.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def StreamTasks(self, request, context):
        """Streamt die aktuelle Warteschlange mit ihren Bewässerungsaufgaben,
        d.h. bei Änderung wird die neue Version übertragen.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CreateJob(self, request, context):
        """Erstelle einen neuen Bewässerungsjob.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetJob(self, request, context):
        """Gibt den Bewässerungsjob mit der gegebenen ID zurück.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetJobs(self, request, context):
        """Gibt alle gespeicherten Bewässerungsjobs zurück.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def UpdateJob(self, request, context):
        """Aktualisiert einen Bewässerungsjob.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def DeleteJob(self, request, context):
        """Lösche den Bewässerungsjob mit der gegebenen ID.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def EnableJob(self, request, context):
        """Aktiviere Bewässerungsjob
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def DisableJob(self, request, context):
        """Deaktiviere Bewässerungsjob
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetAutoMode(self, request, context):
        """Gibt zurück, ob der Automatikmodus aktiviert ist.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def SetAutoMode(self, request, context):
        """Aktiviert/deaktiviert den Automatikmodus.
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetSystemTime(self, request, context):
        """Datum/Uhrzeit/Zeitzone abrufen
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def SetSystemTime(self, request, context):
        """Datum/Uhrzeit einstellen
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def SetSystemTimezone(self, request, context):
        """Zeitzone einstellen
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetDefaultIrrigationTime(self, request, context):
        """Standardzeit bei manueller Bewässerung abrufen
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def SetDefaultIrrigationTime(self, request, context):
        """Standardzeit bei manueller Bewässerung einstellen
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetNZones(self, request, context):
        """Anzahl Bewässerungszonen abrufen
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_TSGRainServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'RequestTask': grpc.unary_unary_rpc_method_handler(
                    servicer.RequestTask,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskRequest.FromString,
                    response_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskRequestResult.SerializeToString,
            ),
            'StartTask': grpc.unary_unary_rpc_method_handler(
                    servicer.StartTask,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskStart.FromString,
                    response_serializer=google_dot_protobuf_dot_wrappers__pb2.BoolValue.SerializeToString,
            ),
            'StopTask': grpc.unary_unary_rpc_method_handler(
                    servicer.StopTask,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskStop.FromString,
                    response_serializer=google_dot_protobuf_dot_wrappers__pb2.BoolValue.SerializeToString,
            ),
            'GetTasks': grpc.unary_unary_rpc_method_handler(
                    servicer.GetTasks,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskList.SerializeToString,
            ),
            'StreamTasks': grpc.unary_stream_rpc_method_handler(
                    servicer.StreamTasks,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.TaskList.SerializeToString,
            ),
            'CreateJob': grpc.unary_unary_rpc_method_handler(
                    servicer.CreateJob,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.Job.FromString,
                    response_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
            ),
            'GetJob': grpc.unary_unary_rpc_method_handler(
                    servicer.GetJob,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.FromString,
                    response_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.Job.SerializeToString,
            ),
            'GetJobs': grpc.unary_unary_rpc_method_handler(
                    servicer.GetJobs,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobList.SerializeToString,
            ),
            'UpdateJob': grpc.unary_unary_rpc_method_handler(
                    servicer.UpdateJob,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.Job.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'DeleteJob': grpc.unary_unary_rpc_method_handler(
                    servicer.DeleteJob,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'EnableJob': grpc.unary_unary_rpc_method_handler(
                    servicer.EnableJob,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'DisableJob': grpc.unary_unary_rpc_method_handler(
                    servicer.DisableJob,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.JobID.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'GetAutoMode': grpc.unary_unary_rpc_method_handler(
                    servicer.GetAutoMode,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=google_dot_protobuf_dot_wrappers__pb2.BoolValue.SerializeToString,
            ),
            'SetAutoMode': grpc.unary_unary_rpc_method_handler(
                    servicer.SetAutoMode,
                    request_deserializer=google_dot_protobuf_dot_wrappers__pb2.BoolValue.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'GetSystemTime': grpc.unary_unary_rpc_method_handler(
                    servicer.GetSystemTime,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=common_dot_grpc__generated_dot_tsgrain__pb2.SystemTime.SerializeToString,
            ),
            'SetSystemTime': grpc.unary_unary_rpc_method_handler(
                    servicer.SetSystemTime,
                    request_deserializer=common_dot_grpc__generated_dot_tsgrain__pb2.Timestamp.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'SetSystemTimezone': grpc.unary_unary_rpc_method_handler(
                    servicer.SetSystemTimezone,
                    request_deserializer=google_dot_protobuf_dot_wrappers__pb2.StringValue.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'GetDefaultIrrigationTime': grpc.unary_unary_rpc_method_handler(
                    servicer.GetDefaultIrrigationTime,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=google_dot_protobuf_dot_wrappers__pb2.Int32Value.SerializeToString,
            ),
            'SetDefaultIrrigationTime': grpc.unary_unary_rpc_method_handler(
                    servicer.SetDefaultIrrigationTime,
                    request_deserializer=google_dot_protobuf_dot_wrappers__pb2.Int32Value.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
            'GetNZones': grpc.unary_unary_rpc_method_handler(
                    servicer.GetNZones,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=google_dot_protobuf_dot_wrappers__pb2.Int32Value.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'TSGRain', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class TSGRain(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def RequestTask(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/RequestTask',
            common_dot_grpc__generated_dot_tsgrain__pb2.TaskRequest.SerializeToString,
            common_dot_grpc__generated_dot_tsgrain__pb2.TaskRequestResult.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def StartTask(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/StartTask',
            common_dot_grpc__generated_dot_tsgrain__pb2.TaskStart.SerializeToString,
            google_dot_protobuf_dot_wrappers__pb2.BoolValue.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def StopTask(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/StopTask',
            common_dot_grpc__generated_dot_tsgrain__pb2.TaskStop.SerializeToString,
            google_dot_protobuf_dot_wrappers__pb2.BoolValue.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetTasks(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/GetTasks',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            common_dot_grpc__generated_dot_tsgrain__pb2.TaskList.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def StreamTasks(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(request, target, '/TSGRain/StreamTasks',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            common_dot_grpc__generated_dot_tsgrain__pb2.TaskList.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CreateJob(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/CreateJob',
            common_dot_grpc__generated_dot_tsgrain__pb2.Job.SerializeToString,
            common_dot_grpc__generated_dot_tsgrain__pb2.JobID.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetJob(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/GetJob',
            common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
            common_dot_grpc__generated_dot_tsgrain__pb2.Job.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetJobs(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/GetJobs',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            common_dot_grpc__generated_dot_tsgrain__pb2.JobList.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def UpdateJob(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/UpdateJob',
            common_dot_grpc__generated_dot_tsgrain__pb2.Job.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def DeleteJob(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/DeleteJob',
            common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def EnableJob(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/EnableJob',
            common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def DisableJob(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/DisableJob',
            common_dot_grpc__generated_dot_tsgrain__pb2.JobID.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetAutoMode(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/GetAutoMode',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            google_dot_protobuf_dot_wrappers__pb2.BoolValue.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def SetAutoMode(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/SetAutoMode',
            google_dot_protobuf_dot_wrappers__pb2.BoolValue.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetSystemTime(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/GetSystemTime',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            common_dot_grpc__generated_dot_tsgrain__pb2.SystemTime.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def SetSystemTime(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/SetSystemTime',
            common_dot_grpc__generated_dot_tsgrain__pb2.Timestamp.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def SetSystemTimezone(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/SetSystemTimezone',
            google_dot_protobuf_dot_wrappers__pb2.StringValue.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetDefaultIrrigationTime(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/GetDefaultIrrigationTime',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            google_dot_protobuf_dot_wrappers__pb2.Int32Value.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def SetDefaultIrrigationTime(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/SetDefaultIrrigationTime',
            google_dot_protobuf_dot_wrappers__pb2.Int32Value.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetNZones(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/TSGRain/GetNZones',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            google_dot_protobuf_dot_wrappers__pb2.Int32Value.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
