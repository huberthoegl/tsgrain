from markupsafe import Markup
from datetime import datetime
from zoneinfo import ZoneInfo

from common import rainrpc
from common.grpc_generated.tsgrain_pb2 import WeekDay
from common.grpc_generated import tsgrain_pb2


def filter_enable(value):
    if value == True:
        return Markup('<td colspan="3">🆗</td>')
    else:
        return Markup('<td colspan="3">🛑</td>')


def filter_zone(zones, zone):
    if zone in zones:
        return Markup("<td> {} </td>".format(zone))
    else:
        return Markup('<td style="color:lightgray;"> {} </td>'.format(zone))


def seconds_to_date_time(seconds: int):
    """Return date and time in a tuple, e.g. ("2024-03-02", "12:45")"""
    dt = datetime.fromtimestamp(seconds).astimezone(ZoneInfo("Europe/Berlin"))
    d = str(dt.date())  # e.g. "2024-03-02"
    t = str(dt.time())[:-3]  # "12:45:00" changed to "12:45"
    return d, t


def filter_date_time(seconds):
    d, t = seconds_to_date_time(seconds)
    return Markup('<td colspan="7">📅 {} &nbsp;&nbsp;&nbsp; ⏰ {}</td>'.format(d, t))


def filter_duration(seconds):
    return Markup('<td colspan="2">{} Min</td>'.format(int(seconds / 60)))


def filter_repeat(r):
    if r == True:
        return Markup('<td colspan="2">W✓</td>')
    else:
        return Markup('<td colspan="2">W−</td>')


def filter_weekday(wday_list, wday):
    if WeekDay.ALL in wday_list:
        return Markup("<td> {} </td>".format(wday))
    elif (
        (wday == "Mo" and WeekDay.MON in wday_list)
        or (wday == "Di" and WeekDay.TUE in wday_list)
        or (wday == "Mi" and WeekDay.WED in wday_list)
        or (wday == "Do" and WeekDay.THU in wday_list)
        or (wday == "Fr" and WeekDay.FRI in wday_list)
        or (wday == "Sa" and WeekDay.SAT in wday_list)
        or (wday == "So" and WeekDay.SUN in wday_list)
    ):
        return Markup("<td> {} </td>".format(wday))
    else:
        return Markup('<td style="color:lightgray;"> {} </td>'.format(wday))


def _short_date(s):
    """s is a datestring, e.g. "2024-03-18" (long, 10 chars) or "24-03-18"
    (short, 8 chars). Always return the short variant.
    """
    if len(s) == 10:
        return s[2:]
    else:
        return s


EDCOLS = 3  # three date strings in a row


def filter_except_days(exception_days):
    n = len(exception_days)
    nrows, rem = divmod(n, EDCOLS)
    if rem > 0:
        nrows += 1
    m = nrows * EDCOLS
    L = ["" for _ in range(m)]
    for i in range(n):
        L[i] = _short_date(exception_days[i])
    s = ""
    for i in range(m):
        r, c = divmod(i, EDCOLS)
        if c == 0:
            s += '<tr> <td colspan="1">❌</td>'
            s += '   <td colspan="2">{}</td>'.format(L[i])
        elif c == EDCOLS - 1:
            s += '   <td colspan="2">{}</td>'.format(L[i])
            s += "</tr>"
        else:
            s += '   <td colspan="2">{}</td>'.format(L[i])
    return Markup(s)


def job_grpc_to_form(job_grpc, job_form):

    if 1 in job_grpc.zones:
        job_form.z1.data = "on"
    else:
        job_form.z1.data = "off"
    if 2 in job_grpc.zones:
        job_form.z2.data = "on"
    else:
        job_form.z2.data = "off"
    if 3 in job_grpc.zones:
        job_form.z3.data = "on"
    else:
        job_form.z3.data = "off"
    if 4 in job_grpc.zones:
        job_form.z4.data = "on"
    else:
        job_form.z4.data = "off"
    if 5 in job_grpc.zones:
        job_form.z5.data = "on"
    else:
        job_form.z5.data = "off"
    if 6 in job_grpc.zones:
        job_form.z6.data = "on"
    else:
        job_form.z6.data = "off"
    if 7 in job_grpc.zones:
        job_form.z7.data = "on"
    else:
        job_form.z7.data = "off"
    if tsgrain_pb2.WeekDay.MON in job_grpc.weekdays:
        job_form.w_mo.data = "on"
    else:
        job_form.w_mo.data = "off"
    if tsgrain_pb2.WeekDay.TUE in job_grpc.weekdays:
        job_form.w_di.data = "on"
    else:
        job_form.w_di.data = "off"
    if tsgrain_pb2.WeekDay.WED in job_grpc.weekdays:
        job_form.w_mi.data = "on"
    else:
        job_form.w_mi.data = "off"
    if tsgrain_pb2.WeekDay.THU in job_grpc.weekdays:
        job_form.w_do.data = "on"
    else:
        job_form.w_do.data = "off"
    if tsgrain_pb2.WeekDay.FRI in job_grpc.weekdays:
        job_form.w_fr.data = "on"
    else:
        job_form.w_fr.data = "off"
    if tsgrain_pb2.WeekDay.SAT in job_grpc.weekdays:
        job_form.w_sa.data = "on"
    else:
        job_form.w_sa.data = "off"
    if tsgrain_pb2.WeekDay.SUN in job_grpc.weekdays:
        job_form.w_so.data = "on"
    else:
        job_form.w_so.data = "off"
    if tsgrain_pb2.WeekDay.ALL in job_grpc.weekdays:
        job_form.w_mo.data = "on"
        job_form.w_di.data = "on"
        job_form.w_mi.data = "on"
        job_form.w_do.data = "on"
        job_form.w_fr.data = "on"
        job_form.w_sa.data = "on"
        job_form.w_so.data = "on"

    job_form.duration.data = str(job_grpc.duration // 60)
    d, t = seconds_to_date_time(job_grpc.date.seconds)
    job_form.date_time_start.data = d + " " + t

    if job_grpc.repeat:
        job_form.repeat.data = "on"
    else:
        job_form.repeat.data = ""

    if job_grpc.enable:
        job_form.enable.data = "on"
    else:
        job_form.enable.data = ""

    for i in range(1, 11):
        exec("job_form.except_day_" + str(i) + ".data = ''")
    for i, s in enumerate(job_grpc.except_days):
        exec("job_form.except_day_" + str(i + 1) + ".data = s")


class FakeJob:
    pass


def job_form_to_grpc(job_form) -> FakeJob:
    """XXX to do The return value is not a valid gRPC Job object because
    weekdays has the rainrpc numbers! Also date.seconds does not exist,
    it is date_seconds.
    """
    # job = tsgrain_pb2.Job()
    job = FakeJob()

    if job_form.repeat.data == "on":
        job.repeat = True
    else:
        job.repeat = False
    if job_form.enable.data == "on":
        job.enable = True
    else:
        job.enable = False
    job.zones = []
    if job_form.z1.data == "on":
        job.zones.append(1)
    if job_form.z2.data == "on":
        job.zones.append(2)
    if job_form.z3.data == "on":
        job.zones.append(3)
    if job_form.z4.data == "on":
        job.zones.append(4)
    if job_form.z5.data == "on":
        job.zones.append(5)
    if job_form.z6.data == "on":
        job.zones.append(6)
    if job_form.z7.data == "on":
        job.zones.append(7)
    # XXX to do: rainrpc.Weekdays.All if all weekdays selected
    job.weekdays = []
    if job_form.w_mo.data == "on":
        job.weekdays.append(rainrpc.Weekdays.Mon)
    if job_form.w_di.data == "on":
        job.weekdays.append(rainrpc.Weekdays.Tue)
    if job_form.w_mi.data == "on":
        job.weekdays.append(rainrpc.Weekdays.Wed)
    if job_form.w_do.data == "on":
        job.weekdays.append(rainrpc.Weekdays.Thu)
    if job_form.w_fr.data == "on":
        job.weekdays.append(rainrpc.Weekdays.Fri)
    if job_form.w_sa.data == "on":
        job.weekdays.append(rainrpc.Weekdays.Sat)
    if job_form.w_so.data == "on":
        job.weekdays.append(rainrpc.Weekdays.Sun)
    dt = datetime.fromisoformat(job_form.date_time_start.data)
    job.date_seconds = dt_secs = int(dt.timestamp())
    job.duration = job_form.duration.data * 60

    job.except_days = []
    for ed in (
        job_form.except_day_1.data,
        job_form.except_day_2.data,
        job_form.except_day_3.data,
        job_form.except_day_4.data,
        job_form.except_day_5.data,
        job_form.except_day_6.data,
        job_form.except_day_7.data,
        job_form.except_day_8.data,
        job_form.except_day_9.data,
        job_form.except_day_10.data,
    ):
        if ed:
            job.except_days.append(ed)
    return job


if __name__ == "__main__":
    pass
