from flask_wtf import FlaskForm, Form
from wtforms import (
    PasswordField,
    StringField,
    IntegerField,
    BooleanField,
    SubmitField,
    FormField,
    DateTimeField,
    SelectField,
    HiddenField,
    validators,
)
from wtforms.validators import DataRequired, Length
from wtforms.widgets import html_params
from markupsafe import Markup  # replaces HTMLString
from wtforms import widgets


# https://gist.github.com/doobeh/239b1e4586c7425e5114
# https://www.w3.org/TR/2011/WD-html5-20110525/the-button-element.html#the-button-element
class ManButtonWidget(object):
    input_type = "submit"
    html_params = staticmethod(html_params)

    def __call__(self, field, **kwargs):
        kwargs.setdefault("id", field.id)
        kwargs.setdefault("type", self.input_type)
        kwargs.setdefault("class", "btn btn-outline-primary")
        # kwargs.setdefault("value", "off")
        if "value" not in kwargs:
            kwargs["value"] = field._value()
        return Markup(
            "<button {params}>{label}</button>".format(
                params=self.html_params(name=field.name, **kwargs),
                label=field.label.text,
            )
        )


class ManButtonField(StringField):
    widget = ManButtonWidget()


class BeregnungsKnoepfe(FlaskForm):
    p1 = ManButtonField("1")
    p2 = ManButtonField("2")
    p3 = ManButtonField("3")
    p4 = ManButtonField("4")
    p5 = ManButtonField("5")
    p6 = ManButtonField("6")
    p7 = ManButtonField("7")


class ZoneButtonWidget(object):
    input_type = "submit"
    html_params = staticmethod(html_params)

    def __call__(self, field, **kwargs):
        kwargs.setdefault("id", field.id)
        kwargs.setdefault("type", self.input_type)
        if field.data in (None, "off", ""):
            kwargs.setdefault("class", "zone_btn btn btn-outline-primary")
            kwargs.setdefault("value", "off")
        else:
            kwargs.setdefault("class", "zone_btn btn btn-primary")
            kwargs.setdefault("value", "on")
        if "value" not in kwargs:
            kwargs["value"] = field._value()
        return Markup(
            "<button {params}>{label}</button>".format(
                params=self.html_params(name=field.name, **kwargs),
                label=field.label.text,
            )
        )


class ZoneButtonField(StringField):
    widget = ZoneButtonWidget()


# XXX differs from ZoneButtonWidget only in wday_btn
class WdayButtonWidget(object):
    input_type = "submit"
    html_params = staticmethod(html_params)

    def __call__(self, field, **kwargs):
        kwargs.setdefault("id", field.id)
        kwargs.setdefault("type", self.input_type)
        if field.data in (None, "off", ""):
            kwargs.setdefault("class", "wday_btn btn btn-outline-primary")
            kwargs.setdefault("value", "off")
        else:
            kwargs.setdefault("class", "wday_btn btn btn-primary")
            kwargs.setdefault("value", "on")
        # kwargs.setdefault("value", "off")
        if "value" not in kwargs:
            kwargs["value"] = field._value()
        mrk = Markup(
            "<button {params}>{label}</button>".format(
                params=self.html_params(name=field.name, **kwargs),
                label=field.label.text,
            )
        )
        return mrk


class WdayButtonField(StringField):
    widget = WdayButtonWidget()


class RepeatCheckboxField(StringField):
    # https://gist.github.com/doobeh/4668212
    # https://wtforms.readthedocs.io/en/3.1.x/widgets/
    # The 'checked' HTML attribute is set if the field’s data is a non-false value.
    widget = widgets.CheckboxInput()
    # Die Unterstriche bei data_enable_testmich werden durch - ersetzt!!!
    # widget.html_params(bla="dings", data_enable_testmich="true")


class EnableCheckboxField(StringField):
    widget = widgets.CheckboxInput()


def zonevalidator(form, field):
    if (
        form.z1.data == "off"
        and form.z2.data == "off"
        and form.z3.data == "off"
        and form.z4.data == "off"
        and form.z5.data == "off"
        and form.z6.data == "off"
        and form.z7.data == "off"
    ):
        form.z1.errors = ["Bitte mindestens eine der Zonen aktivieren."]
        raise validators.ValidationError("zonevalidate error")


def wdayvalidator(form, field):
    if (
        form.w_mo.data == "off"
        and form.w_di.data == "off"
        and form.w_mi.data == "off"
        and form.w_do.data == "off"
        and form.w_fr.data == "off"
        and form.w_sa.data == "off"
        and form.w_so.data == "off"
    ):
        form.w_mo.errors = ["Bitte mindestens einen Wochentag aktivieren."]
        raise validators.ValidationError("wdayvalidate error")


class JobForm(FlaskForm):
    # zonevalidator for Zone 1 validates if at least one of the Z1-Z7 are on
    z1 = ZoneButtonField("1", validators=[zonevalidator])
    z2 = ZoneButtonField("2")
    z3 = ZoneButtonField("3")
    z4 = ZoneButtonField("4")
    z5 = ZoneButtonField("5")
    z6 = ZoneButtonField("6")
    z7 = ZoneButtonField("7")
    duration = IntegerField()
    date_time_start = StringField(
        validators=[DataRequired(message="Bitte Datum/Zeit auswählen")]
    )
    repeat = RepeatCheckboxField()
    enable = EnableCheckboxField()
    w_mo = WdayButtonField("Mo", validators=[wdayvalidator])
    w_di = WdayButtonField("Di")
    w_mi = WdayButtonField("Mi")
    w_do = WdayButtonField("Do")
    w_fr = WdayButtonField("Fr")
    w_sa = WdayButtonField("Sa")
    w_so = WdayButtonField("So")
    except_day_1 = StringField()
    except_day_2 = StringField()
    except_day_3 = StringField()
    except_day_4 = StringField()
    except_day_5 = StringField()
    except_day_6 = StringField()
    except_day_7 = StringField()
    except_day_8 = StringField()
    except_day_9 = StringField()
    except_day_10 = StringField()

    # No form fields:
    # zone_set_all = ButtonField("alle setzen")
    # zone_clear_all = ButtonField("alle löschen")


class JobsOverviewForm(FlaskForm):
    pass
