
function my_xhr_request() {
    console.log("myfunction() called");
    var username=document.getElementById("inputUsername").value;  
    var password=document.getElementById("inputPassword").value;  
    console.log("username: %s password: %s", username, password);

    // test param()
    // var z = param({ 'key1': 'value1', 'key2': 'value2' });
    // console.log("z =", z); // key1=value1&key2=value2

    var xhr = new XMLHttpRequest();
    // POST requests encodes data in request body
    xhr.open('POST', '/signUpUser');
    // xhr.open('POST', '/signUpUser?bla1=ignore1&bla2=ignore2');
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        console.log("response: [%s] (type %s)", xhr.responseText, typeof xhr.responseText);
        // response: { "pass": "bar", "status: "OK", "user": "foo" }
        if (xhr.status === 200) {  
            // OK
            console.log('status 200'); 
        }
        else {
            // Error
            console.log('status nicht 200');
        }
    };
    data = { 'username': username, 'password': password };
    xhr.send(encodeURI(param(data)));  // body 
}


// takes an object and return URL encoded string
function param(object) {
    var encodedString = '';
    for (var prop in object) {
        if (object.hasOwnProperty(prop)) {
            if (encodedString.length > 0) {
                encodedString += '&';
            }
            encodedString += encodeURI(prop + '=' + object[prop]);
        }
    }
    return encodedString;
}



