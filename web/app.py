from flask import Flask, Response, render_template, request, redirect, url_for, json

from flask_wtf.csrf import CSRFProtect

from grpc._channel import _InactiveRpcError

from common import rainrpc
from common.grpc_generated import tsgrain_pb2

from filter import (
    filter_enable,
    filter_zone,
    filter_date_time,
    filter_duration,
    filter_repeat,
    filter_weekday,
    filter_except_days,
    job_grpc_to_form,
    job_form_to_grpc,
)

from datetime import date, datetime, timedelta
import logging
import sys
import forms

app = Flask(__name__)

app.logger.setLevel(logging.INFO)
# turn off request logging
logging.getLogger('werkzeug').disabled = True

app.jinja_env.filters["enable"] = filter_enable
app.jinja_env.filters["zone"] = filter_zone
app.jinja_env.filters["date_time"] = filter_date_time
app.jinja_env.filters["duration"] = filter_duration
app.jinja_env.filters["repeat"] = filter_repeat
app.jinja_env.filters["weekday"] = filter_weekday
app.jinja_env.filters["except_days"] = filter_except_days


csrf = CSRFProtect()
csrf.init_app(app)

# python -c 'import secrets; print(secrets.token_hex())'
app.config["SECRET_KEY"] = (
    "8be07c9752b59f39dbd7d2d2f24e0bb3bd8a982663f2889a7fad90cf6be6fa7f"
)


@app.route("/")
def index():
    return render_template("index.html", title="Tsgrain")
    # https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-ii-templates
    # return render_template('index.html', title='Home', user=user)
    # return "Hello World!"


@app.route("/man", methods=["GET", "POST"])
def man():
    man_btns = forms.BeregnungsKnoepfe()

    try:
        stub = rainrpc.open()
        response = rainrpc.get_def_irr_time(stub)
        duration = response.value // 60  # seconds to minutes
    except _InactiveRpcError as e:
        return redirect(url_for("error", ecode=1))

    return render_template("man.html", man_btns=man_btns, duration=duration)


# Aufruf optional mit /job?job_id=<int>
@app.route("/job", methods=["GET", "POST"])
def job():
    job_form = forms.JobForm()

    job_id = None
    if "job_id" in request.args:
        # URL /job/job_id=<id>
        ### print("**** POST (UPDATE JOB) ****")
        job_id = int(request.args.get("job_id"))
    else:
        # URL /job
        ### print("**** POST (CREATE JOB) ****")
        pass

    if request.method == "POST":

        if job_form.validate_on_submit():

            job = job_form_to_grpc(job_form)

            if job_id:
                jobid = job_id
            else:
                jobid = 0

            try:
                stub = rainrpc.open()
                if job_id:
                    rainrpc.update_job(
                        stub,
                        jobid,
                        job.date_seconds,
                        job.duration,
                        job.zones,
                        job.enable,
                        job.repeat,
                        job.weekdays,
                        job.except_days,
                    )
                    app.logger.info("update_job {}".format(jobid))
                else:
                    rainrpc.create_job(
                        stub,
                        jobid,
                        job.date_seconds,
                        job.duration,
                        job.zones,
                        job.enable,
                        job.repeat,
                        job.weekdays,
                        job.except_days,
                    )
                    app.logger.info("create_job {}".format(jobid))

            except _InactiveRpcError as e:
                return redirect(url_for("error", ecode=2))

            return redirect(url_for("index"))
    else:
        ### print("--- job GET view ---")

        try:
            stub = rainrpc.open()
            duration_man = rainrpc.get_def_irr_time(stub).value // 60
        except _InactiveRpcError as e:
            return redirect(url_for("error", ecode=3))

        if job_id:
            # update existing job
            # URL job/job_id=<int>
            try:
                stub = rainrpc.open()
                job = rainrpc.get_job(stub, job_id)
            except _InactiveRpcError as e:
                return redirect(url_for("error", ecode=4))

            job_grpc_to_form(job, job_form)
        else:
            # Create fresh new job
            job_form.z1.data = "on"
            job_form.z2.data = "on"
            job_form.z3.data = "on"
            job_form.z4.data = "on"
            job_form.z5.data = "on"
            job_form.z6.data = "on"
            job_form.z7.data = "on"
            job_form.w_mo.data = "on"
            job_form.w_di.data = "on"
            job_form.w_mi.data = "on"
            job_form.w_do.data = "on"
            job_form.w_fr.data = "on"
            job_form.w_sa.data = "on"
            job_form.w_so.data = "on"
            job_form.duration.data = str(15)  # min
            job_form.repeat.data = "on"
            job_form.enable.data = "on"
            # format looks like "2024-12-31 23:00"
            # tomorrow midnight
            job_form.date_time_start.data = (
                str(date.today() + timedelta(days=1)) + " 00:00"
            )

    # Render page to
    # - create new job
    # - update existing job
    # - display validation errors
    return render_template("job.html", job_form=job_form, job_id=job_id)


@app.route("/jobs", methods=["GET", "POST"])
def jobs():

    if request.method == "POST":

        if request.form["submit"] == "abbrechen":
            ### print("--> POST abbrechen")
            return render_template("index.html", title="Tsgrain")

        if request.form["submit"] == "löschen":
            job_id = int(request.form["job_id"])
            ### print("--> POST loeschen", job_id)
            try:
                stub = rainrpc.open()
                rainrpc.delete_job(stub, job_id)
            except _InactiveRpcError as e:
                return redirect(url_for("error", ecode=5))

            return redirect(url_for("jobs"))
            # return render_template("index.html", title="Tsgrain")

        if request.form["submit"] == "ändern":
            job_id = int(request.form["job_id"])
            ### print("--> POST aendern", type(job_id), job_id)

            # Beispiel-URL: GET /job?job_id=3
            return redirect(url_for("job", job_id=job_id))

    else:
        # GET
        token = jobs_form = forms.JobsOverviewForm().csrf_token
        try:
            stub = rainrpc.open()
            myjobs = rainrpc.get_all_jobs(stub)
            # Jinja placeholders in jobs.html support attributes job.id, job.date.seconds,
            # job.duration, job.zones, job.enable, job.repeat, job.weekdays,
            # job.except_days
            app.logger.info("get_all_jobs")
            return render_template("jobs.html", jobs_form=myjobs.jobs, token=token)

        except _InactiveRpcError as e:
            # No connection to GRPC Server, maybe Controller down
            return redirect(url_for("error", ecode=6))


@app.route("/set", methods=["GET", "POST"])
def set():
    # job_form = forms.JobForm()
    # if job_form.validate_on_submit():
    #    pass
    return render_template("set.html")


# XHR, fetch(), man.html
@app.route("/man_ajax", methods=["POST"])
def man_ajax():
    # request.is_json
    # content type ct = request.headers.get("Content-Type")
    if request.json == {}:
        # get tasks status
        try:
            stub = rainrpc.open()
            task_list = rainrpc.get_all_tasks(stub)

            n = len(task_list.tasks)
            if n == 0:
                # print("no tasks")
                pass
            # for i, task in enumerate(task_list.tasks):
            #     print(
            #         f"Task[{i}]",
            #         "src:",
            #         task.source,
            #         "zone:",
            #         task.zone_id,
            #         "duration:",
            #         task.duration,
            #         "start:",
            #         task.datetime_started.seconds,
            #         "fini:",
            #         task.datetime_finished.seconds,
            #         "now:",
            #         task_list.now.seconds,
            #         "auto_mode:",
            #         task_list.auto_mode,
            #     )
            my_tasks = [
                {
                    "source": t.source,
                    "zone_id": t.zone_id,
                    "duration": t.duration,
                    "datetime_started": t.datetime_started.seconds,
                    "datetime_finished": t.datetime_finished.seconds,
                    "now": task_list.now.seconds,
                    "auto_mode": task_list.auto_mode,
                }
                for t in task_list.tasks
            ]
            resp_data = json.dumps({"status": "status_ok", "task_list": my_tasks})
        except _InactiveRpcError as e:
            # No connection to GRPC Server, maybe Controller down
            resp_data = json.dumps({"status": "_InactiveRpcError"})

    elif "button" in request.json:
        try:
            ### print("request_button:", request.json["button"])
            zone = int(request.json["button"])
            stub = rainrpc.open()
            result = rainrpc.request_task(
                stub,
                src="manual",
                zone=zone,
                duration=300,
                queuing=False,
                cancelling=True,
            )
            ### print("result", "started:", result.started, "stopped:", result.stopped)
            app.logger.info("request_task {}".format(zone))
            resp_data = json.dumps({"status": "button todo"})
        except _InactiveRpcError as e:
            resp_data = json.dumps({"status": "_InactiveRpcError"})
    else:
        resp_data = json.dumps({"status": "todo"})
    resp = Response(response=resp_data, status=200, mimetype="application/json")
    return resp

    # Alternative to force application/json:
    # from flask import jsonify
    # return jsonify(somedict)
    # https://sentry.io/answers/flask-getting-post-data/


@app.route("/error", methods=["GET"])
def error():
    if "ecode" in request.args:
        error_code = int(request.args.get("ecode"))
    else:
        error_code = "None"
    return render_template("error.html", error_code=error_code)


if __name__ == "__main__":
    app.run()
