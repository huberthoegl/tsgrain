# requirements.txt
#
# x86_64 PC
# Python 3.10.13 (conda-forge) 

tinydb~=4.8.0
cyra~=1.0.2
schedule~=1.2.1
smbus~=1.1.post2
protobuf~=4.25.3
grpcio~=1.62.0
flask~=3.0.0
flask-wtf~=1.2.1
