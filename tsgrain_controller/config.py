# coding=utf-8
import cyra


class Config(cyra.Config):
    builder = cyra.ConfigBuilder()

    builder.comment("Anzahl Bewaesserungszonen")
    n_zones = builder.define("n_zones", 7)

    builder.comment("Pfad der Datenbankdatei")
    db_path = builder.define("db_path", "")

    builder.comment("Manuelle Bewaesserungszeit in Sekunden")
    manual_time = builder.define("manual_time", 300)

    builder.comment("Port für GRPC-Kommunikation")
    grpc_port = builder.define("grpc_port", "[::]:50051")

    builder.comment("Debug-Ausgaben loggen")
    log_debug = builder.define("log_debug", False)

    builder.push("io")

    builder.comment("ID des I2C-Bus")
    i2c_bus_id = builder.define("i2c_bus_id", 0)

    builder.comment("GPIO-Pin, mit dem der Interrupt-Pin des MCP23017 verbunden ist")
    gpio_interrupt = builder.define("gpio_interrupt", 17)

    builder.comment("Entprellzeit in Sekunden")
    gpio_delay = builder.define("gpio_delay", 0.05)

    builder.pop()

    builder.comment("Befehle für Konfiguration von Datum/Uhrzeit")
    builder.push("timecfg")

    builder.comment("Datum/Uhrzeit setzen. Verwende Python-strftime()-Platzhalter.")
    cmd_set_datetime = builder.define("cmd_set_datetime", "date -s '%Y-%m-%d %H:%M:%S'")

    builder.comment("Zeitzone im Unix-Format abrufen")
    cmd_get_timezone = builder.define("cmd_get_timezone", "cat /etc/timezone")

    builder.comment("Zeitzone setzen. Verwende {TZ} als Platzhalter für die Zeitzone.")
    cmd_set_timezone = builder.define(
        "cmd_set_timezone", "timedatectl set-timezone '{TZ}'"
    )

    builder.pop()

    builder.comment("Ausgaenge")
    output_devices = builder.define(
        "output_devices",
        {
            "VALVE_1": "0x27/B0/!",
            "VALVE_2": "0x27/B1/!",
            "VALVE_3": "0x27/B2/!",
            "VALVE_4": "0x27/B3/!",
            "VALVE_5": "0x27/B4/!",
            "VALVE_6": "0x27/B5/!",
            "VALVE_7": "0x27/B6/!",
            "LED_Z_1": "0x27/A0",
            "LED_Z_2": "0x27/A1",
            "LED_Z_3": "0x27/A2",
            "LED_Z_4": "0x27/A3",
            "LED_Z_5": "0x27/A4",
            "LED_Z_6": "0x27/A5",
            "LED_Z_7": "0x27/A6",
            "LED_M_AUTO": "0x23/B0",
            "LED_M_MAN": "0x23/B1",
        },
    )

    builder.comment("Eingaenge")
    input_devices = builder.define(
        "input_devices",
        {
            "BT_Z_1": "0x23/A0/!",
            "BT_Z_2": "0x23/A1/!",
            "BT_Z_3": "0x23/A2/!",
            "BT_Z_4": "0x23/A3/!",
            "BT_Z_5": "0x23/A4/!",
            "BT_Z_6": "0x23/A5/!",
            "BT_Z_7": "0x23/A6/!",
            "BT_MODE": "0x23/A7/!",
        },
    )
