import signal
import sys
import os
import traceback
import argparse
import pathlib

from tsgrain_controller import __version__, application
from tsgrain_controller.io import io_factory


def _get_io_types() -> str:
    io_types = [t.name for t in io_factory.IoType]
    return "|".join(io_types)


def _parse_args():
    parser = argparse.ArgumentParser(description="TSGRain Controller")
    parser.add_argument(
        "--config", dest="cfg", type=pathlib.Path, help="Pfad der Konfigurationsdatei"
    )

    parser.add_argument("--io", dest="io", help=f"IO-Typ: {_get_io_types()}")
    return parser.parse_args()


def run():
    print(f"TSGRain Controller {__version__}\n")
    args = _parse_args()

    io_type = io_factory.IoType.MCP23017
    if args.io:
        try:
            io_type = io_factory.IoType[args.io.upper()]
        except KeyError:
            print(f"IO-Typ {args.io} ungültig. Verwende {_get_io_types()}")
            sys.exit(1)

    cfg_path = None
    if args.cfg:
        cfg_path = args.cfg.resolve()

    app = application.Application(io_type, cfg_path=cfg_path)

    def _signal_handler(sig, frame):  # pylint: disable=unused-argument
        app.stop()
        print("Exited.")
        sys.exit(0)

    def _except_handler(etype, value, tb):
        print("_except_handler")
        traceback.print_exception(etype, value, tb)
        os.kill(os.getpid(), signal.SIGKILL)

    signal.signal(signal.SIGINT, _signal_handler)
    signal.signal(signal.SIGTERM, _signal_handler)
    sys.excepthook = _except_handler

    app.start()

    signal.pause()


if __name__ == "__main__":
    run()
