# Nach einem Beispiel von 
# http://www.netzmafia.de/skripten/hardware/RasPi/Projekt-OnOff/index.html

import subprocess
import time
import RPi.GPIO as GPIO

# shutdown without pwd must be enabled in /etc/sudoers
# # User privilege specification
# root    ALL=(ALL:ALL) ALL
# tsgrain ALL=(ALL:ALL) ALL
# tsgrain ALL=NOPASSWD: /sbin/shutdown  
# tsgrain ALL=NOPASSWD: /usr/bin/systemctl


# Zeitdauer des Tastendrucks
duration = 0


def handler(pin):
    global duration

    if not GPIO.input(pin):
        # Taste gedrueckt
        if duration == 0:
            duration = time.time()
    else:
        # Taste losgelassen
        if duration > 0:
            elapsed = (time.time() - duration)
            duration = 0
            if elapsed >= 3:  # laenger als 3 Sekunden 
                subprocess.call(['sudo', 'shutdown', '-h', 'now'], shell=False) 
            elif elapsed >= 0.1: # Entprellzeit
                # 0.1 bis < 3 sek
                subprocess.call(['sudo', 'shutdown', '-r', 'now'], shell=False) 

