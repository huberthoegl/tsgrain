# coding=utf-8
from enum import Enum
from tsgrain_controller import models, io


class IoType(Enum):
    NONE = 0
    MOCKUI = 1
    MCP23017 = 2


# pylint: disable=import-outside-toplevel
def new_io(app: models.AppInterface, io_type: IoType) -> io.Io:
    if io_type == IoType.MCP23017:
        try:
            from tsgrain_controller.io import mcp23017 as io_mod
        except ImportError:
            from tsgrain_controller.io import mockui as io_mod
    elif io_type == IoType.MOCKUI:
        from tsgrain_controller.io import mockui as io_mod
    else:
        from tsgrain_controller import io as io_mod  # pylint: disable=reimported

    return io_mod.Io(app)
