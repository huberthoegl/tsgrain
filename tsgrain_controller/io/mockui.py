# coding=utf-8

import math
from typing import Dict, Optional

import os
from tsgrain_controller import io, util, models

FIFO_OUT = "/tmp/fifo_out"
FIFO_IN = "/tmp/fifo_in"
try:
    os.mkfifo(FIFO_OUT)
    os.mkfifo(FIFO_IN)
except FileExistsError as e:
    pass


class Io(util.StoppableThread, io.Io):
    def __init__(self, app: models.AppInterface):
        super().__init__(0.01)
        self.app = app
        self.logger = app.get_logger()

        self._outputs: Dict[str, bool] = {}

        print("*******************************")
        print("*** Start tsgrain_mockui.py ***")
        print("*******************************")

        try:
            self.fifo_out = os.open(FIFO_OUT, os.O_WRONLY)
            self.fifo_in = os.open(FIFO_IN, os.O_RDONLY | os.O_NONBLOCK)
        except FileNotFoundError as e:
            pass

    def setup(self):
        pass

    def write_output(self, key: str, val: bool):
        """called on change of state"""
        self._outputs[key] = val
        try:
            # This call blocks, i.e. tsgrain_mockup.py must be started first
            os.write(self.fifo_out, bytes(f"{key} {val}\n", "ascii"))
        except BrokenPipeError as e:
            # if tsgrain_mockup.py terminates too early
            pass

    def run_cycle(self):
        """called about 100 times/sec"""
        try:
            x = os.read(self.fifo_in, 1024)
            if len(x) > 0:
                s = x.decode().strip()
                if s == "BT_MODE":
                    self._trigger_cb("BT_MODE")
                    self.logger.info("received BT_MODE from fifo")
                elif s in (
                    "BT_Z_1",
                    "BT_Z_2",
                    "BT_Z_3",
                    "BT_Z_4",
                    "BT_Z_5",
                    "BT_Z_6",
                    "BT_Z_7",
                ):
                    self._trigger_cb(s)
        except BlockingIOError as e:
            pass

    def cleanup(self):
        pass
