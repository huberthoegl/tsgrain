# coding=utf-8
from typing import Callable, Optional


class Io:

    def __init__(self, *args):  # pylint: disable=unused-argument
        self.cb_input: Optional[Callable[[str], None]] = None

    def set_callback(self, cb: Optional[Callable[[str], None]]):
        """
        Setze die Callback-Funktion, die bei einer Eingabe aufgerufen wird.
        Als Parameter wird der Name des Eingabegeräts mit übergeben.

        :param cb: Input-Callback-Funktion
        """
        self.cb_input = cb

    def _trigger_cb(self, key: str):
        """
        Löse die Input-Callback-Funktion aus

        :param key: Gerätename
        """
        if self.cb_input is not None:
            self.cb_input(key)

    def start(self):
        """Initialisiere die IO"""

    def stop(self):
        """Beende die IO und deaktiviere alle Ausgabegeräte"""

    def write_output(self, key: str, val: bool):
        """
        Setze den Zustand eines Ausgabegeräts

        :param key: Name des Ausgabegeräts (z.B. ``VALVE_1``)
        :param val: Status des Ausgabegeräts
        """
