# Teileliste 

* Raspberry Pi 3B or 3B+ ([1] oder [2], ca. 40 Euro)

  https://www.reichelt.de/raspberry-pi-3-b-4x-1-4-ghz-1-gb-ram-wlan-bt-raspberry-pi-3b--p217696.html


* MeanWell MDR-20-5 Hutschienen-Netzteil (DIN-Rail) 5V, 3A 15W 
  Bei [1] ca. 15 Euro.

  https://www.reichelt.de/schaltnetzteil-hutschiene-15-w-5-v-3-a-snt-mw-mdr20-05-p85234.html

* Spannungswandler von 5V auf 3.3V, so dass der 3.3V Ausgang des Raspberry Pi
  nicht benötigt wird.  

  Bei [1]: DC/DC-Wandler, 0,75 W, 3,3 V, SIL 4 (0.75S4A_0503S3RP)

  https://www.reichelt.de/dc-dc-wandler-0-75-w-3-3-v-sil-4-0-75s4a-0503s3rp-p242889.html

  Anmerkung: Es würde auch ein preiswerter LDO-Regler funktionieren.

* Trafo mit 24 V Wechselspannung, ca. 40 bis 100 Watt. Der Trafo liefert 
  bei uns die Spannung für die Magnetventile. Ein Magnetventil benötigt 
  etwa 10 bis 20 Watt Leistung, wieviel Watt der Trafo liefern muss hängt 
  also ab davon, wie viele Magnetventile man gleichzeitig aktivieren möchte. 
  Im Normalfall ist zu einem Zeitpunkt immer nur ein Ventil eingeschaltet.
  Unser Trafo ist von der Firma Elma TT, https://www.transformatoren.at.

* Port Expander Modul mit MCP23017. Bei Reichelt [1] gibt es das
  "DEBO IO BOARD", das ca. 5 Euro kostet.

  https://www.reichelt.de/entwicklerboards-i-o-erweiterungsboard-mcp23017-debo-io-board-p266040.html

  Alle Tasten, LEDs und Ventile werden über diese Expander gesteuert. Je
  nachdem, wie viele Plätze zu beregnen sind, braucht man einen, zwei oder noch
  mehr dieser Expander (wir haben sieben Plätze und benötigen zwei Stück).

* DS3231 Real-Time Clock Modul ([3], ca. 5 Euro)

  https://www.az-delivery.de/products/ds3231-real-time-clock

* Relais Modul mit 8 Kanälen zum Schalten der Magnetventile, je Kanal ein 
  Opto-Koppler ([3], ca. 8 Euro). Es gibt auch Module mit 16 Relais.

  https://www.az-delivery.de/products/8-relais-modul

* Für die Handsteuerung benötigt man für die sieben Plätze sieben Kippschalter
  mit Mittelstellung, z.B. von [1] oder [2].

* Frontplatte 

  - Sieben grosse Taster für die Beregnung der Plätze.
  - Sieben grüne LEDs die jeweils über den Tasten angeordnet werden.
  - Ein Taster für die Funktion "Automatik-Aus/Ein" 
  - Eine dreifarbige Status-LED 
    (z.B. LED LL 5-8000RGB RGB-LED gemeinsame Kathode bei [1], auch bei [2]).

* Kunststoffgehäuse von Europa-Components, 330mm x 250mm x 130mm, IP65, 
  aufklappbarer Deckel, Teilenummer PBE332513 (bei [4], ca. 50 Euro)

Lieferfirmen:

[1] https://www.reichelt.de 

[2] https://www.pollin.de

[3] https://www.az-delivery.de

[4] https://www.farnell.de



