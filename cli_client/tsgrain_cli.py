import argparse
import sys
from common import rainrpc
from google.protobuf import empty_pb2
from google.protobuf.wrappers_pb2 import Int32Value
import time
from datetime import datetime, timezone
from zoneinfo import ZoneInfo


def create_job_cmd(args):
    # print("create_job_cmd:", args)

    # args.date is formatted like "2024-02-23T23:30:00"
    t = datetime.fromisoformat(args.date)
    ts = int(t.timestamp())
    zones = rainrpc._zones_convert(args.zones)
    rainrpc.create_job(
        stub,
        args.id,
        ts,
        args.duration,
        zones,
        args.enable,
        args.repeat,
        args.weekdays,
        args.except_days,
    )


def update_job_cmd(args):
    # print("update_job_cmd:", args)
    t = datetime.fromisoformat(args.date)
    ts = int(t.timestamp())
    zones = rainrpc.zones_convert(args.zones)
    rainrpc.update_job(
        stub,
        args.id,
        ts,
        args.duration,
        zones,
        args.enable,
        args.repeat,
        args.weekdays,
        args.except_days,
    )


def req_task_cmd(args):
    if not 1 <= args.zone <= 7:
        print("zone id must be in the range 1 to 7")
        return
    r = rainrpc.request_task(
        stub,
        src=args.src,
        zone=args.zone,
        duration=args.duration,
        queuing=args.queuing,
        cancelling=args.cancelling,
    )
    print("TaskRequestResult:", r)


def start_task_cmd(args):
    if not 1 <= args.zone <= 7:
        print("zone id must be in the range 1 to 7")
        return
    r = rainrpc.start_task(
        stub, src=args.src, zone=args.zone, duration=args.duration, queuing=args.queuing
    )
    print("Response (bool):", r.value)


def stop_task_cmd(args):
    if not 1 <= args.zone <= 7:
        print("zone id must be in the range 1 to 7")
        return
    r = rainrpc.stop_task(stub, src=args.src, zone=args.zone)
    print("Response (bool):", r.value)


if __name__ == "__main__":
    if False:
        import sys

        T = (["all", "1,2"], ["1-7"], ["1", "2", "3"], ["1-3,6"], ["1-3,5-7"])
        for z in T:
            r = rainrpc._zones_convert(z)
            # print(r)
        sys.exit(1)

    parser = argparse.ArgumentParser()

    # dest='subcommands' leads to Namespace(subcommands='task' ...)
    sub = parser.add_subparsers(
        title="subcommands (run 'SUBCMD -h' for help)", dest="subcommands"
    )

    create_job_parser = sub.add_parser("create-job", help="create job")
    create_job_parser.set_defaults(func=create_job_cmd)
    create_job_parser.add_argument(
        "--id",
        type=int,
        action="store",
        dest="id",
        default=1,
        help="job id (def. 1)",
    )
    create_job_parser.add_argument(
        "--date",
        action="store",
        dest="date",
        default="2024-02-27T15:30:00",  # XXX todo: add reasonable value
        help="start date (example: 2024-02-23T23:30:00)",
    )
    create_job_parser.add_argument(
        "--duration",
        type=int,
        action="store",
        dest="duration",
        default=rainrpc.DFLT_TIME,
        help=f"select duration in sec (def. {rainrpc.DFLT_TIME})",
    )
    create_job_parser.add_argument(
        "--zones",
        type=str,
        nargs="+",
        action="store",
        dest="zones",
        default=["All"],
        metavar="ZONE",
        help="select zones (def. 'All') examples: '1 2 3',  '1,5,6', '1-7', '1-3,6', '1-3,5-7', '1-7', 'All'",
    )
    create_job_parser.add_argument(
        "--enable",
        type=bool,
        action="store",
        dest="enable",
        default=False,
        metavar="BOOL",
        help="enable job (def. False)",
    )
    create_job_parser.add_argument(
        "--repeat",
        type=bool,
        action="store",
        dest="repeat",
        default=False,
        metavar="BOOL",
        help="repeat job (def. False)",
    )
    create_job_parser.add_argument(
        "--weekdays",
        type=rainrpc.Weekdays,
        # choices=list(rainrpc.Weekdays),
        nargs="+",
        action="store",
        dest="weekdays",
        default=[rainrpc.Weekdays.All],
        metavar="Weekday",
        help="job active on weekdays (def. 'All'), example: --weekdays Mon Tue Wed Sun",
    )
    create_job_parser.add_argument(
        "--except-days",
        type=str,
        nargs="+",
        action="store",
        dest="except_days",
        metavar="DAY",
        help="job inactive at these days, example: --except_days 2024-06-18 2024-07-30",
    )

    update_job_parser = sub.add_parser("update-job", help="update job")
    update_job_parser.set_defaults(func=update_job_cmd)
    update_job_parser.add_argument(
        "--id",
        type=int,
        action="store",
        dest="id",
        default=1,
        help="job id (def. 1)",
    )
    update_job_parser.add_argument(
        "--date",
        action="store",
        dest="date",
        help="start date (example: 2024-02-23T23:30:00)",
    )
    update_job_parser.add_argument(
        "--duration",
        type=int,
        action="store",
        dest="duration",
        default=rainrpc.DFLT_TIME,
        help=f"select duration in sec (def. {rainrpc.DFLT_TIME})",
    )
    update_job_parser.add_argument(
        "--zones",
        type=str,
        nargs="+",
        action="store",
        dest="zones",
        default=["all"],
        metavar="ZONE",
        help="select zones (def. 'all') examples: 2 4 6   1,5,6  2,3,5-7   1-3,6   1-7   all",
    )
    update_job_parser.add_argument(
        "--enable",
        type=bool,
        action="store",
        dest="enable",
        default=False,
        help="enable job (def. False)",
    )
    update_job_parser.add_argument(
        "--repeat",
        type=bool,
        action="store",
        dest="repeat",
        default=False,
        help="repeat job (def. False)",
    )
    update_job_parser.add_argument(
        "--weekdays",
        type=rainrpc.Weekdays,
        # choices=list(rainrpc.Weekdays),
        nargs="+",
        action="store",
        dest="weekdays",
        default=[rainrpc.Weekdays.All],
        metavar="Weekday",
        help="select weekdays (def. All), example: --weekdays Mon Tue Wed Sun",
    )
    update_job_parser.add_argument(
        "--except-days",
        type=str,
        nargs="+",
        action="store",
        dest="except_days",
        metavar="DAY",
        help="job not active days, example: --except_days 2024-06-18 2024-07-30",
    )

    req_task_parser = sub.add_parser("req-task", help="request task")
    req_task_parser.set_defaults(func=req_task_cmd)
    req_task_parser.add_argument(
        "--src",
        type=rainrpc.Source,
        choices=list(rainrpc.Source),
        action="store",
        dest="src",
        default=rainrpc.Source.manual,
        help="select source (def. manual)",
    )
    req_task_parser.add_argument(
        "--zone",
        type=int,
        action="store",
        dest="zone",
        default=1,
        help="select zone (1-7, def. 1)",
    )
    req_task_parser.add_argument(
        "--duration",
        type=int,
        action="store",
        dest="duration",
        default=rainrpc.DFLT_TIME,
        help=f"select duration in sec (def. {rainrpc.DFLT_TIME})",
    )
    req_task_parser.add_argument(
        "--queuing",
        type=bool,
        action="store",
        dest="queuing",
        default=False,
        help="select queuing (def. False)",
    )
    req_task_parser.add_argument(
        "--cancelling",
        type=bool,
        action="store",
        dest="cancelling",
        default=False,
        help="select cancelling (def. False)",
    )

    start_task_parser = sub.add_parser("start-task", help="start task")
    start_task_parser.set_defaults(func=start_task_cmd)
    start_task_parser.add_argument(
        "--src",
        type=rainrpc.Source,
        choices=list(rainrpc.Source),
        action="store",
        dest="src",
        default=rainrpc.Source.manual,
        help="select source (def. manual)",
    )
    start_task_parser.add_argument(
        "--zone",
        type=int,
        action="store",
        dest="zone",
        default=1,
        help="select zone (1-7, def. 1)",
    )
    start_task_parser.add_argument(
        "--duration",
        type=int,
        action="store",
        dest="duration",
        default=rainrpc.DFLT_TIME,
        help=f"select duration in sec (def. {rainrpc.DFLT_TIME})",
    )
    start_task_parser.add_argument(
        "--queuing",
        type=bool,
        action="store",
        dest="queuing",
        default=False,
        help="select queuing (def. False)",
    )

    stop_task_parser = sub.add_parser("stop-task", help="stop task")
    stop_task_parser.set_defaults(func=stop_task_cmd)
    stop_task_parser.add_argument(
        "--src",
        type=rainrpc.Source,
        choices=list(rainrpc.Source),
        action="store",
        dest="src",
        default=rainrpc.Source.manual,
        help="select source (def. manual)",
    )
    stop_task_parser.add_argument(
        "--zone",
        type=int,
        action="store",
        dest="zone",
        default=1,
        help="select zone (1-7, def. 1)",
    )

    parser.add_argument("-a", action="store_true", dest="auto", help="auto mode")
    parser.add_argument("-m", action="store_true", dest="manual", help="manual mode")
    parser.add_argument(
        "-M", action="store_true", dest="get_mode", help="check for auto mode"
    )
    parser.add_argument(
        "-T", action="store_true", dest="get_all_tasks", help="get state of all tasks"
    )
    parser.add_argument(
        "-J",
        action="store_true",
        dest="get_all_jobs",
        help="get state of all jobs (from JSON-DB)",
    )
    parser.add_argument(
        "-j",
        type=int,
        action="store",
        dest="get_job",
        # default=1,  # do not set default value, otherwise option is always taken
        metavar="JOBID",
        help="get job state (no default)",
    )
    parser.add_argument(
        "--delete-job",
        type=int,
        action="store",
        dest="delete_job",
        # default=1,  # do not set default value, otherwise option is always taken
        metavar="JOBID",
        help="delete job (no default)",
    )
    parser.add_argument(
        "--enable-job",
        type=int,
        action="store",
        dest="enable_job",
        # default=1,  # do not set default value, otherwise option is always taken
        metavar="JOBID",
        help="enable job (no default)",
    )
    parser.add_argument(
        "--disable-job",
        type=int,
        action="store",
        dest="disable_job",
        # default=1,  # do not set default value, otherwise option is always taken
        metavar="JOBID",
        help="disable job (no default)",
    )
    parser.add_argument(
        "--get-n-zones",
        action="store_true",
        dest="get_n_zones",
        help="get number of zones",
    )
    parser.add_argument(
        "--set-def-irr-time",
        action="store",
        type=str,
        metavar="TIME",
        dest="set_def_irr_time",
        help="set default irrigation time [sec]",
    )
    parser.add_argument(
        "--get-def-irr-time",
        action="store_true",
        dest="get_def_irr_time",
        help="get default irrigation time [sec]",
    )
    parser.add_argument(
        "--get-system-time",
        action="store_true",
        dest="get_system_time",
        help="get system date/time/tz",
    )
    parser.add_argument(
        "--set-system-time",
        action="store",
        type=str,
        metavar="ISOTIME",
        dest="set_system_time",
        help="set system date/time, e.g. '2024-02-23T23:30:00'",
    )
    parser.add_argument(
        "--set-system-timezone",
        action="store",
        dest="set_system_timezone",
        help="set system timezone (e.g. 'Europe/Berlin')",
    )

    args = parser.parse_args(args=None if sys.argv[1:] else ["-h"])
    # Alternatives:
    # args=(sys.argv[1:] or ['--help'])
    # None if sys.argv[1:] else ['-h']
    # parser.print_help()

    stub = rainrpc.open()

    try:
        # options
        if args.auto:
            rainrpc.set_auto_mode(stub, True)
        elif args.manual:
            rainrpc.set_auto_mode(stub, False)
        elif args.get_mode:
            rainrpc.get_mode(stub)
        elif args.get_all_tasks:
            task_list = rainrpc.get_all_tasks(stub)
            print("TaskList.tasks ->", task_list.tasks)
            print("TaskList.now ->", task_list.now)
            print("TaskList.auto_mode ->", task_list.auto_mode)
        elif args.get_all_jobs:
            joblist = rainrpc.get_all_jobs(stub)
            for job in joblist.jobs:
                print(job)
        elif args.get_job:
            job = rainrpc.get_job(stub, int(args.get_job))
            print(job)
        elif args.delete_job:
            rainrpc.delete_job(stub, int(args.delete_job))
        elif args.enable_job:
            rainrpc.enable_job(stub, int(args.enable_job))
        elif args.disable_job:
            rainrpc.disable_job(stub, int(args.disable_job))
        elif args.get_n_zones:
            response = stub.GetNZones(empty_pb2.Empty())
            print(response.value)
        elif args.set_def_irr_time:
            d = int(args.set_def_irr_time)
            print("d =", d)
            response = stub.SetDefaultIrrigationTime(Int32Value(value=d))
            print(response)
        elif args.get_def_irr_time:
            response = rainrpc.get_def_irr_time(stub)
            print(response)
        elif args.get_system_time:
            response = stub.GetSystemTime(empty_pb2.Empty())
            ts = response.datetime
            tz = response.timezone  # "Europe/Berlin"
            # print(type(ts), ts.seconds)
            # print(type(tz), tz)
            dt = datetime.utcfromtimestamp(ts.seconds).astimezone(ZoneInfo(tz))
            print(dt)
            # print(dt.utcoffset())
        elif args.set_system_time:
            print("XXX to do")
        elif args.set_system_timezone:
            print("XXX to do")
        # subcommands
        else:
            # subcommands have func defined by set_defaults(func=...)
            args.func(args)

    except _InactiveRpcError as e:
        print("_InactiveRpcError")

    rainrpc.close()
