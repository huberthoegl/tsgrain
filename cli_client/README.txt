
$ python tsgrain_cli.py stop-task --zone 7

Achtung: 

- Die Option --enable ist zwar boolean, jedoch ist "--enable False" immer
  noch True. Zum Eischalten der Option muss man "--enable True" schreiben. 
  Wenn man die Option komplett weglaesst heisst das False (default). 

  Genauso bei --repeat.  

  Besser waere das --enable und --no-enable, sowie --repeat und --no-repeat zu nennen.
  Das waehlt man durch

     parser.add_argument('--feature', action=argparse.BooleanOptionalAction)

  https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse

 
- Bei create-job ist die Job ID (z.B. --id=2) ohne Bedeutung fuer die generierte 
  Job ID.
