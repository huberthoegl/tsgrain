python tsgrain_cli.py create-job --id=1 --date 2024-03-17T21:50:00 --duration 120 --zones All \
   --enable True --repeat True --weekdays All --except-days 2024-06-18 2024-06-25 \
   2024-07-01 2024-07-08
