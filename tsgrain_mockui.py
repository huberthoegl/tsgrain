''' tsgrain_mockui.py

H. Hoegl, <Hubert.Hoegl@hs-augsburg.de>, 2024
'''

import threading
import time
import os
import sys
import tkinter as tk
from tkinter import ttk
from tkinter import Frame, Label, Entry

pack_args = {"padx": 5, "pady": 5}
exit_flag = False

# https://levelup.gitconnected.com/inter-process-communication-between-node-js-and-python-2e9c4fda928d
# mkfifo /tmp/fifo_out   # controller to mockui
# mkfifo /tmp/fifo_in    # mockui to controller

# see tsgrain_controller/io/mockui.py


# controller sends data to mockup
fifo_out = os.open("/tmp/fifo_out", os.O_RDONLY | os.O_NONBLOCK)

print("Now start controller in another shell window:")
print("python -m tsgrain_controller --config tsgrain.toml --io MOCKUI")

# mockup sends data to controller (pressed keys)
# This call blocks! If os.O_NONBLOCK is given, the following error results:
# OSError: [Errno 6] No such device or address: '/tmp/fifo_in'
fifo_in = os.open("/tmp/fifo_in", os.O_WRONLY)


def thread_function(n):
    while not exit_flag:
        try:
            x = os.read(fifo_out, 1024)
            if len(x) > 0:
                # print(">", x)
                L = outputs(x)
                for key, value in L:
                    if key in led_dict:
                        if value:
                            col = "red"
                        else:
                            col = "grey"
                        led_dict[key]["bg"] = col
                # print(L)
        except BlockingIOError as e:
            pass
        time.sleep(0.01)


i = 0


def my_function():
    global i
    i += 1
    if i % 2 == 0:
        led1["bg"] = "black"
    else:
        led1["bg"] = "red"
    # repeat function after 100ms (0.1s)
    form.after(100, my_function)


def outputs(bstr):
    """
    bstr sieht so aus:
    b"VALVE_1 False\nVALVE_2 False\nVALVE_3 False\nVALVE_4 False\nLED_Z_2 False\n"
    """
    t = bstr.decode()
    L = []
    for key_val in t.splitlines():
        # XXX todo: seems that p can be empty (?)
        p = key_val.split()
        L.append((p[0], True if p[1] == "True" else False))
    return L


def button1(*_):
    print("BT_Z_1")
    os.write(fifo_in, bytes("BT_Z_1", "ascii"))


def button2(*_):
    print("BT_Z_2")
    os.write(fifo_in, bytes("BT_Z_2", "ascii"))


def button3(*_):
    print("BT_Z_3")
    os.write(fifo_in, bytes("BT_Z_3", "ascii"))


def button4(*_):
    print("BT_Z_4")
    os.write(fifo_in, bytes("BT_Z_4", "ascii"))


def button5(*_):
    print("BT_Z_5")
    os.write(fifo_in, bytes("BT_Z_5", "ascii"))


def button6(*_):
    print("BT_Z_6")
    os.write(fifo_in, bytes("BT_Z_6", "ascii"))


def button7(*_):
    print("BT_Z_7")
    os.write(fifo_in, bytes("BT_Z_7", "ascii"))


def button_m(*_):
    print("BT_MODE")
    os.write(fifo_in, bytes("BT_MODE", "ascii"))


def exit(*_):
    global exit_flag
    exit_flag = True
    sys.exit(0)


if __name__ == "__main__":
    root = tk.Tk()
    root.title("Tsgrain MockUI")
    # root.geometry('640x280+200+200')
    form = Frame()

    label_m = Label(form, text="A")
    label_m.grid(row=0, column=0, **pack_args)

    label1 = Label(form, text="P1")
    label1.grid(row=0, column=1)

    label2 = Label(form, text="P2")
    label2.grid(row=0, column=2)

    label3 = Label(form, text="P3")
    label3.grid(row=0, column=3)

    label4 = Label(form, text="P4")
    label4.grid(row=0, column=4)

    label5 = Label(form, text="P5")
    label5.grid(row=0, column=5)

    label6 = Label(form, text="P6")
    label6.grid(row=0, column=6)

    label7 = Label(form, text="P7")
    label7.grid(row=0, column=7)

    # "LED"
    led_m_man = tk.Label(form, text=" M ", bg="grey")
    led_m_man.grid(row=1, column=0, **pack_args)
    led_m_auto = tk.Label(form, text=" A ", bg="grey")
    led_m_auto.grid(row=3, column=0, **pack_args)

    led1 = tk.Label(form, text="   ", bg="grey")
    led1.grid(row=1, column=1)
    led2 = tk.Label(form, text="   ", bg="grey")
    led2.grid(row=1, column=2)
    led3 = tk.Label(form, text="   ", bg="grey")
    led3.grid(row=1, column=3)
    led4 = tk.Label(form, text="   ", bg="grey")
    led4.grid(row=1, column=4)
    led5 = tk.Label(form, text="   ", bg="grey")
    led5.grid(row=1, column=5)
    led6 = tk.Label(form, text="   ", bg="grey")
    led6.grid(row=1, column=6)
    led7 = tk.Label(form, text="   ", bg="grey")
    led7.grid(row=1, column=7)

    valve1 = tk.Label(form, text="   ", bg="grey")
    valve1.grid(row=3, column=1)
    valve2 = tk.Label(form, text="   ", bg="grey")
    valve2.grid(row=3, column=2)
    valve3 = tk.Label(form, text="   ", bg="grey")
    valve3.grid(row=3, column=3)
    valve4 = tk.Label(form, text="   ", bg="grey")
    valve4.grid(row=3, column=4)
    valve5 = tk.Label(form, text="   ", bg="grey")
    valve5.grid(row=3, column=5)
    valve6 = tk.Label(form, text="   ", bg="grey")
    valve6.grid(row=3, column=6)
    valve7 = tk.Label(form, text="   ", bg="grey")
    valve7.grid(row=3, column=7)

    mybutton_m = ttk.Button(form, command=button_m, text="--A↕--", default="active")
    mybutton_m.grid(row=5, column=0)

    mybutton1 = ttk.Button(form, command=button1, text="-- 1 --", default="active")
    mybutton1.grid(row=5, column=1)
    mybutton2 = ttk.Button(form, command=button2, text="-- 2 --", default="active")
    mybutton2.grid(row=5, column=2)
    mybutton3 = ttk.Button(form, command=button3, text="-- 3 --", default="active")
    mybutton3.grid(row=5, column=3)
    mybutton4 = ttk.Button(form, command=button4, text="-- 4 --", default="active")
    mybutton4.grid(row=5, column=4)
    mybutton5 = ttk.Button(form, command=button5, text="-- 5 --", default="active")
    mybutton5.grid(row=5, column=5)
    mybutton6 = ttk.Button(form, command=button6, text="-- 6 --", default="active")
    mybutton6.grid(row=5, column=6)
    mybutton7 = ttk.Button(form, command=button7, text="-- 7 --", default="active")
    mybutton7.grid(row=5, column=7)

    exit_button = ttk.Button(form, command=exit, text="EXIT", default="active")
    exit_button.grid(row=6, column=0, **pack_args)

    # run function first time
    # my_function()

    led_dict = {
        "LED_Z_1": led1,
        "LED_Z_2": led2,
        "LED_Z_3": led3,
        "LED_Z_4": led4,
        "LED_Z_5": led5,
        "LED_Z_6": led6,
        "LED_Z_7": led7,
        "VALVE_1": valve1,
        "VALVE_2": valve2,
        "VALVE_3": valve3,
        "VALVE_4": valve4,
        "VALVE_5": valve5,
        "VALVE_6": valve6,
        "VALVE_7": valve7,
        "LED_M_AUTO": led_m_auto,
        "LED_M_MAN": led_m_man,
    }

    x = threading.Thread(target=thread_function, args=(1,))
    x.start()

    form.pack()
    form.mainloop()
    x.join()
